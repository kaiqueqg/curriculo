﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticHelpers {

    //enumHelpers
    public static UnitObj.DefendStyle Forward(this UnitObj.DefendStyle value)
    {
        switch(value)
        {
            case UnitObj.DefendStyle.Arrow:
                return UnitObj.DefendStyle.Line;
            case UnitObj.DefendStyle.Line:
                return UnitObj.DefendStyle.Arrow;
            default:
                return UnitObj.DefendStyle.Arrow;
        }
    }

    public static UnitObj.FormationStyle Forward(this UnitObj.FormationStyle value)
    {
        switch(value)
        {
            case UnitObj.FormationStyle.Arrow:
                return UnitObj.FormationStyle.Line;
            case UnitObj.FormationStyle.Line:
                return UnitObj.FormationStyle.Column;
            case UnitObj.FormationStyle.Column:
                return UnitObj.FormationStyle.Arrow;
            default:
                return UnitObj.FormationStyle.Arrow;
        }
    }

    public static UnitObj.ROE Forward(this UnitObj.ROE value)
    {
        switch(value)
        {
            case UnitObj.ROE.AlwaysAttack:
                return UnitObj.ROE.DontAttack;
            case UnitObj.ROE.DontAttack:
                return UnitObj.ROE.AlwaysAttack;
            default:
                return UnitObj.ROE.AlwaysAttack;
        }
    }

    //float helpers
    public static float ZeroBetween(this float value, float from, float to)
    {
        if(value > to || value < from) return value;
        else return 0;
    }

    public static bool IsBetween(this float value, float from, float to)
    {
        if(value > to || value < from) return false;
        else return true;
    }

    public static float Map(this float value, float fromSource, float toSource, float fromTarget, float toTarget)
    {
        return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
    }

    //int helpers
    public static int Map(this int value, int fromSource, int toSource, int fromTarget, int toTarget)
    {
        return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
    }

    //string helpers
    public static string BracketContent(this string text, char closeBracket)
    {
        int v = 0;
        string rtnString = "";
        char openBracket = text[0];

        v++;
        text = text.Substring(1);

        while(v > 0 && text.Length > 0)
        {
            if(text[0] == closeBracket)
            {
                v--;
            }
            else if(text[0] == openBracket)
            {
                v++;
            }

            if(v > 0) rtnString += text.Substring(0, 1);
            text = text.Substring(1);
        }

        return rtnString;
    }

    public static string BracketContent(this string text, char openBracket, char closeBracket)
    {
        int v = 0;
        string rtnString = "";
        while(v == 0)
        {
            if(text[0] == openBracket)
                v = 1;
            text = text.Substring(1);
        }

        while(v > 0 && text.Length > 0)
        {
            if(text[0] == closeBracket)
            {
                v--;
            }
            else if(text[0] == openBracket)
            {
                v++;
            }

            if(v > 0) rtnString += text.Substring(0, 1);
            text = text.Substring(1);
        }

        return rtnString;
    }

    public static string BracketContent(this string text, int openBracketPos, char closeBracket)
    {
        int v = 0;
        string rtnString = "";
        char openBracket = text[openBracketPos];

        v++;
        text = text.Substring(1);

        while(v > 0 && text.Length > 0)
        {
            if(text[0] == closeBracket)
            {
                v--;
            }
            else if(text[0] == openBracket)
            {
                v++;
            }

            if(v > 0) rtnString += text.Substring(0, 1);
            text = text.Substring(1);
        }

        return rtnString;
    }

    public static string RemoveBrackedContent(this string text, out string brackedContent, char closeBracket)
    {
        int v = 1;
        char openBracket = text[0];
        brackedContent = "";
        text = text.Substring(1);

        while(v > 0 && text.Length > 0)
        {
            if(text[0] == openBracket) v++;
            else if(text[0] == closeBracket) v--;

            if(v > 0) brackedContent += text[0];
            text = text.Substring(1);
        }
        return text;
    }
    
    public static T[] GetSlice<T>(this T[] data, int start)
    {
        T[] result = new T[data.GetLength(0)-start];
        Array.Copy(data, start, result, 0, data.GetLength(0) - start);
        return result;
    }

    public static T[] GetSlice<T>(this T[] data, int start, int length)
    {
        T[] result = new T[length];
        Array.Copy(data, start, result, 0, length);
        return result;
    }
}
