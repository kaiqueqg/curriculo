﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

    public float time;

	void Update () {
        Invoke("BackToMainMenu", time);

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            BackToMainMenu();
        }
	}

    private void BackToMainMenu()
    {
        SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
    }
}
