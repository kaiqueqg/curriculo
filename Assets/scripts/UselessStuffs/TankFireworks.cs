﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFireworks : MonoBehaviour {

    public GameObject f;
    public GameObject axis;

    void Start () {
        InvokeRepeating("Shot", 0f, 3f);
	}

    private void Shot()
    {
        Instantiate(f, axis.transform);
    }
}
