﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditorBuildingAlign : MonoBehaviour {

    public enum type { house, hotel, skyscraper};
    Vector3 lastPos;
    public bool randomSize;
    public bool startSet = true;
    public type myType;

    void Start ()
    {
        
    }
	
	void Update ()
    {
        if(lastPos != transform.position) transform.position = new Vector3(Mathf.Round(transform.position.x) + 0.5f, 0, Mathf.Round(transform.position.z) + 0.5f);

        if(startSet)
        {
            startSet = false;
            transform.localScale = new Vector3(transform.localScale.x-0.001f, transform.localScale.y, transform.localScale.z - 0.001f);
        }

        if(randomSize && myType == type.skyscraper)
        {
            randomSize = false;
            transform.localScale = new Vector3(transform.localScale.x - 0.001f, Mathf.Round(Random.Range(3f, 10f)), transform.localScale.z - 0.001f);
            transform.position = new Vector3(Mathf.Round(transform.position.x)+0.5f, 0, Mathf.Round(transform.position.z) + 0.5f);
        }
        
        lastPos = transform.position;
    }
}
