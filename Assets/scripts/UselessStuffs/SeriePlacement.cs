﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SeriePlacement : MonoBehaviour {
    
    public float objectSize;
    public GameObject prefab;
    
    public bool on = false;
    public int amountOfObj = 0;
    public bool debug = false;

    List<GameObject> l = new List<GameObject>();
    Vector3 startPos;

    void Start()
    {
        startPos = transform.position;
    }

    void Update ()
    {
        if(on)
        {
        //AdjustSize
            if(l.Count != amountOfObj)
            {
                if(debug)Debug.Log("Difference Size on Serie Placement");
                DestroyAll();

                for(int i = 0; i < amountOfObj; i++)
                {
                    l.Add(Instantiate(prefab, transform));
                }
            }

            if(l.Count > 0)
            {
                //Positioning
                Vector3 dir = transform.forward;
                l[0].transform.position = transform.position;
                l[0].transform.rotation = Quaternion.LookRotation(dir);
                Vector3 pos = transform.position + (dir * objectSize);
                for(int i = 1; i < l.Count; i++)
                {
                    l[i].transform.position = pos;
                    l[i].transform.rotation = Quaternion.LookRotation(dir);
                    pos += (dir * objectSize);
                }
            }
        }
        else
        {
            DestroyAll();
        }
	}

    void DestroyAll()
    {
        if(debug) Debug.Log("All destroyed");

        for(int i = 0; i < l.Count; i++)
        {
            DestroyImmediate(l[i]);
        }

        l = new List<GameObject>();
    }
}
