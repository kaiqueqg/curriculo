﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class NoDecimalPosAlign : MonoBehaviour {

    public bool executeNonStop = false;
    public bool doIt = true;

	void Update ()
    {
        if(doIt)
        {
            doIt = executeNonStop;

            transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
        }
	}
}
