﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TreePlacer : MonoBehaviour {

    public bool doIt = false;
    public GameObject o;
    public float frontMin = 5;
    public float frontMax = 10;
    public float sideRange = 5;
    public float height = 0;
    public int amount = 0;

    private List<GameObject> l = new List<GameObject>();
    
    void Update()
    {
        if(o != null)
        {
            if(l.Count != amount)
            {
                for(int i = 0; i < l.Count; i++)
                {
                    if(l[i] != null) DestroyImmediate(l[i]);
                }

                l = new List<GameObject>();

                for(int i = 0; i < amount; i++)
                {
                    l.Add(Instantiate(o, gameObject.transform));
                }
            }

            Vector3 pos;
            float step = 0;
            for(int i = 0; i < l.Count; i++)
            {
                pos = new Vector3(step, height, Random.Range(-sideRange, sideRange));

                l[i].transform.localPosition = pos;
                l[i].transform.Rotate(Vector3.up, Random.Range(0, 259));

                step += Random.Range(frontMin, frontMax);
            }
        }
    }
}
