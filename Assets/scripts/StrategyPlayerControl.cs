﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StrategyPlayerControl : MonoBehaviour
{
    //Camera
    public CameraControl cameraControl;
    public Camera mainCamera;
    public GameObject cameraContainer;
    public float cameraDistance;
    public float cameraMinZoom;
    public float cameraMaxZoom;
    public float cameraMovementRate;
    public float cameraSpinRate;
    public float zoomRate;

    public GameObject[] constructions;
    public GameObject[] ghostConstructions;

    public GameObject[] units;
    public GameObject[] ghostUnits;

    public Canvas gameMenu;

    public GameObject pointMesh;

    //Map
    public int mapSize;
    public float mapPrecision;
    public AStar aStar;
    public AStarCell[,] map;
    public float MapMinHeight(float x, float z)
    {
        return aStar.MinHeight(map, x, z, mapPrecision);
    }

    //UI
    public Canvas mainCanvas;
    public GameObject introText;
    public GameObject winText;
    public GameObject defeatText;
    public Image fadePanel;

    //ButtonOptions
    public Button[] unitOptions;
    public Button[] constructionOptions;
    public Button constructorOption;
    public Button[] dropzoneOptions;

    //Units selected "styles"
    public UnitObj.DefendStyle defendStyle;
    public UnitObj.FormationStyle moveStyle;
    public UnitObj.ROE roe;

    //Resource control
    private int points = 2;
    public int GetPoints() { return points; }
    public void PointsUpdate(int v) { points += v; }
    public GameObject pointsText;
    
    //EnemySpawn
    public GameObject enemyObj;
    public GameObject blackHoleEffect;
    public Text nextEnemyPanel;
    private float enemyMinSpawnTime = 60f;
    private float enemySpawnTime = 60f;
    private List<GameObject> enemiesAvailable;
    public bool inMainMenu;

    //UnitsCount
    public int tankAvailable = 0;
    public GameObject dropZone = null;

    //Game start
    public bool gameStarted = false;
    public GameObject tankAirdrop;
    public GameObject dropZoneUnitAirdrop;
    public GameObject startCamera;
    public GameObject gameCamera;
    public GameObject startTankOne;
    public GameObject startTankTwo;
    
    //temp
    public float formationTightness = 0.9f;
    public GameObject ballPrefab;
    public GameObject cilinderPrefab;
    public GameObject squarePrefab;
    
    public void Awake()
    {
        //Application.targetFrameRate = 60;
        //Time.timeScale = 0.1f;

        defendStyle = UnitObj.DefendStyle.Line;
        moveStyle = UnitObj.FormationStyle.Arrow;
        roe = UnitObj.ROE.AlwaysAttack;
        aStar = new AStar();
        enemiesAvailable = new List<GameObject>();

        float[] heightTest = new float[2] { 0.5f, 1.5f };
        map = aStar.GetMap(mapSize, mapPrecision, heightTest, LayerMask.GetMask("Building"));

        //for(int i = 0; i < map.GetLength(0); i++)
        //{
        //    for(int j = 0; j < map.GetLength(1); j++)
        //    {
        //        if(map[i, j].MinHeight() != 0.5f)
        //        {
        //            DrawSquare(10f, 1, Color.red, map[i, j].Pos() * mapPrecision);
        //        }
        //        else
        //        {
        //            DrawSquare(10f, 1, Color.green, map[i, j].Pos() * mapPrecision);
        //        }

        //    }
        //}

        cameraMinZoom = 10f;
        cameraMaxZoom = 50f;
        cameraMovementRate = 2f;
        cameraSpinRate = 8f;
        zoomRate = 16f;

        if(!inMainMenu) cameraDistance = mainCamera.transform.position.y.Map(cameraMinZoom, cameraMaxZoom, 0, 100);
        if(!inMainMenu) cameraControl = new CameraControl(this);

        points = 2;

        StartGame();
    }
    
    public void Update()
    {
        if(inMainMenu)
        {
            enemySpawnTime -= Time.deltaTime;
            if(enemySpawnTime < 0f)
            {
                enemySpawnTime = enemyMinSpawnTime;
            }
        }
        else
        {
            cameraControl.Update();

            if(Input.GetKey(KeyCode.Escape))
            {
                Time.timeScale = 0;
                gameMenu.gameObject.SetActive(true);
            }

            enemySpawnTime -= Time.deltaTime;
            nextEnemyPanel.text = enemySpawnTime.ToString("0") + "s";
            if(enemySpawnTime < 0f)
            {
                enemySpawnTime = enemyMinSpawnTime;
                StartCoroutine(InstantiateEnemy(true, new Vector3()));
            }

            for(int i = 0; i < enemiesAvailable.Count; i++)
            {
                if(enemiesAvailable[i] == null)
                {
                    enemiesAvailable.RemoveAt(i);
                }
            }

            if(gameStarted && enemiesAvailable.Count == 0)
            {
                gameStarted = false;
                StartCoroutine(Win());
            }
            
            if(gameStarted && tankAvailable <= 0)
            {
                if(dropZone == null || points < 1)
                {
                    gameStarted = false;
                    StartCoroutine(Defeat());
                }
            }
        }
    }

    public void StartGame()
    {
        //Application.stackTraceLogType = StackTraceLogType.None;

        if(inMainMenu)
        {
            enemySpawnTime = 4f;
            enemyMinSpawnTime = 4f;
        }
        else
        {
            StartCoroutine(InstantiateEnemy(true, new Vector3()));
            StartCoroutine(InstantiateEnemy(true, new Vector3()));
            StartCoroutine(ChangeCamera());
        }
    }

    private void SpawnDropZoneUnit()
    {
        GameObject spawnVehicle = Instantiate(units[1], transform.position, transform.rotation);

        UnitObj u = spawnVehicle.GetComponent<UnitObj>();
        u.GetComponent<UnitObj>().player = this;
    }

    IEnumerator Defeat()
    {
        fadePanel.gameObject.SetActive(true);
        defeatText.SetActive(true);
        yield return new WaitForSeconds(4f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Defeat");

        while(!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    IEnumerator Win()
    {
        fadePanel.gameObject.SetActive(true);
        winText.SetActive(true);
        yield return new WaitForSeconds(4f);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Win");

        while(!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    IEnumerator ChangeCamera()
    {
        yield return new WaitForSeconds(10f);

        startCamera.SetActive(false);
        gameCamera.SetActive(true);
        gameStarted = true;
        introText.SetActive(false);

        yield return null;
    }

    IEnumerator InstantiateEnemy(bool random, Vector3 place)
    {
        Vector3 p = place;

        if(random)
        {
            p = new Vector3(Mathf.Round(Random.Range(318, 465)), 0f, Mathf.Round(Random.Range(251, 464)));
            while(map[(int)(p.x / mapPrecision), (int)(p.z / mapPrecision)].MinHeight() != 0.5f)
            {
                p = new Vector3(Mathf.Round(Random.Range(318, 465)), 0f, Mathf.Round(Random.Range(251, 464)));
                //Debug.Log("Chosing new place to spawn enemy beside: " + place.ToString());
            }
        }
        
        Destroy(Instantiate(blackHoleEffect, p, Quaternion.identity), 2f);

        yield return new WaitForSeconds(1);

        GameObject o;
        o = Instantiate(enemyObj, p, Quaternion.identity);
        enemiesAvailable.Add(o);

        if(o != null)
        {
            UnitObj u = o.GetComponent<UnitObj>();
            if(u != null) u.player = this;
        }

        yield return null;
    }

    public void ButtonPressed(int i)
    {
        cameraControl.ButtonPressed(i);
    }

    public GameObject GetInst(Object o)
    {
        return (GameObject)Instantiate(o);
    }

    public void DisableAllButtons()
    {
        foreach(Button b in unitOptions)
        {
            b.gameObject.SetActive(false);
        }
        foreach(Button b in constructionOptions)
        {
            b.gameObject.SetActive(false);
        }
        foreach(Button b in dropzoneOptions)
        {
            b.gameObject.SetActive(false);
        }
        constructorOption.gameObject.SetActive(false);
    }

    public void DestroyObject(GameObject o)
    {
        Destroy(o);
    }

    public void RecalculateLandMap()
    {

    }
    
    public Vector3[] ArrowFormation(Vector3 pos, Vector3 frontDir, int nFollowers, int nSupportFollowers, float minH)
    {
        Vector3[] fPos = new Vector3[nFollowers];
        Vector3[] sPos = new Vector3[nSupportFollowers];

        Vector3 sidePos = pos;
        Vector3 sideDirec = Quaternion.Euler(0, 135, 0) * frontDir.normalized;

        Vector3 otherSidePos = pos;
        Vector3 otherSideDirec = Quaternion.Euler(0, -135, 0) * frontDir.normalized;

        Vector3 sSidePos = pos - frontDir.normalized * 15f;
        Vector3 sSideDirec = Quaternion.Euler(0, 135, 0) * frontDir.normalized;

        Vector3 sOtherSidePos = pos - frontDir.normalized * 15f;
        Vector3 sOtherSideDirec = Quaternion.Euler(0, -135, 0) * frontDir.normalized;

        float step = formationTightness;
        int sideNumber;
        int sSideNumber;
        
        if(nFollowers % 2 != 0f) sideNumber = nFollowers / 2 + 1; else sideNumber = nFollowers / 2;
        int otherSideNumber = nFollowers / 2;

        if(nSupportFollowers % 2 != 0f) sSideNumber = nSupportFollowers / 2 + 1; else sSideNumber = nSupportFollowers / 2;
        int sOtherSideNumber = nSupportFollowers / 2;

        int c = 0;
        for(int i = 0; i < sideNumber; i++, c++)
        {
            sidePos = sidePos + (sideDirec * step);
            sidePos.y = 0;
            Collider[] co = Physics.OverlapBox(sidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sidePos.x);
            int zPos = (int)Mathf.Round(sidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                fPos[c] = sidePos;
            }
            else
            {
                fPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }

        int sC = 0;
        for(int i = 0; i < sSideNumber; i++, sC++)
        {
            sSidePos = sSidePos + (sSideDirec * step);
            sSidePos.y = 0;
            Collider[] sCo = Physics.OverlapBox(sSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sSidePos.x);
            int zPos = (int)Mathf.Round(sSidePos.z);

            if(sCo.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                sPos[sC] = sSidePos;
            }
            else
            {
                sPos[sC] = new Vector3(-1f, -1f, -1f);
            }
        }

        for(int i = 0; i < otherSideNumber; i++, c++)
        {
            otherSidePos = otherSidePos + (otherSideDirec * step);
            otherSidePos.y = 0;
            Collider[] co = Physics.OverlapBox(otherSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));
            int xPos = (int)Mathf.Round(otherSidePos.x);
            int zPos = (int)Mathf.Round(otherSidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                fPos[c] = otherSidePos;
            }
            else
            {
                fPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }

        for(int i = 0; i < sOtherSideNumber; i++, sC++)
        {
            sOtherSidePos = sOtherSidePos + (sOtherSideDirec * step);
            sOtherSidePos.y = 0;
            Collider[] sCo = Physics.OverlapBox(sOtherSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sOtherSidePos.x);
            int zPos = (int)Mathf.Round(sOtherSidePos.z);

            if(sCo.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                sPos[sC] = sOtherSidePos;
            }
            else
            {
                sPos[sC] = new Vector3(-1f, -1f, -1f);
            }
        }

        Vector3[] returnValue = new Vector3[nFollowers + nSupportFollowers];
        int returnValueIndex = 0;
        for(int i = 0; i < fPos.GetLength(0); i++, returnValueIndex++)
        {
            returnValue[returnValueIndex] = fPos[i];
        }
        for(int i = 0; i < sPos.GetLength(0); i++, returnValueIndex++)
        {
            returnValue[returnValueIndex] = sPos[i];
        }

        return returnValue;
    }
    public Vector3[] LineFormation(Vector3 pos, Vector3 frontDir, int nFollowers, int nSupportFollowers, float minH)
    {
        Vector3[] fPos = new Vector3[nFollowers];
        Vector3[] sPos = new Vector3[nSupportFollowers];

        Vector3 sidePos = pos;
        Vector3 sideDirec = Quaternion.Euler(0, 90, 0) * frontDir.normalized;

        Vector3 otherSidePos = pos;
        Vector3 otherSideDirec = Quaternion.Euler(0, -90, 0) * frontDir.normalized;

        Vector3 sSidePos = pos - frontDir.normalized* 15f;
        Vector3 sSideDirec = Quaternion.Euler(0, 90, 0) * frontDir.normalized;

        Vector3 sOtherSidePos = pos - frontDir.normalized * 15f;
        Vector3 sOtherSideDirec = Quaternion.Euler(0, -90, 0) * frontDir.normalized;

        float step = formationTightness;
        int sideNumber;
        int sSideNumber;

        if(nFollowers % 2 != 0f) sideNumber = nFollowers / 2 + 1; else sideNumber = nFollowers / 2;
        int othersideNumber = nFollowers / 2;

        if(nSupportFollowers % 2 != 0f) sSideNumber = nSupportFollowers / 2 + 1; else sSideNumber = nSupportFollowers / 2;
        int sOthersideNumber = nSupportFollowers / 2;

        int c = 0;
        for(int i = 0; i < sideNumber; i++, c++)
        {
            sidePos = sidePos + (sideDirec * step);
            sidePos.y = 0;
            Collider[] co = Physics.OverlapBox(sidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sidePos.x);
            int zPos = (int)Mathf.Round(sidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                fPos[c] = sidePos;
            }
            else
            {
                fPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }
        for(int i = 0; i < othersideNumber; i++, c++)
        {
            otherSidePos = otherSidePos + (otherSideDirec * step);
            otherSidePos.y = 0;
            Collider[] co = Physics.OverlapBox(otherSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));
            int xPos = (int)Mathf.Round(otherSidePos.x);
            int zPos = (int)Mathf.Round(otherSidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                fPos[c] = otherSidePos;
            }
            else
            {
                fPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }

        c = 0;
        for(int i = 0; i < sSideNumber; i++, c++)
        {
            sSidePos = sSidePos + (sSideDirec * step);
            sSidePos.y = 0;
            Collider[] co = Physics.OverlapBox(sSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sSidePos.x);
            int zPos = (int)Mathf.Round(sSidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                sPos[c] = sSidePos;
            }
            else
            {
                sPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }
        for(int i = 0; i < sOthersideNumber; i++, c++)
        {
            sOtherSidePos = sOtherSidePos + (sOtherSideDirec * step);
            sOtherSidePos.y = 0;
            Collider[] co = Physics.OverlapBox(sOtherSidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));
            int xPos = (int)Mathf.Round(sOtherSidePos.x);
            int zPos = (int)Mathf.Round(sOtherSidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                sPos[c] = sOtherSidePos;
            }
            else
            {
                sPos[c] = new Vector3(-1f, -1f, -1f);
            }
        }

        Vector3[] returnValue = new Vector3[nFollowers + nSupportFollowers];
        int returnValueIndex = 0;
        for(int i = 0; i < fPos.GetLength(0); i++, returnValueIndex++)
        {
            returnValue[returnValueIndex] = fPos[i];
        }
        for(int i = 0; i < sPos.GetLength(0); i++, returnValueIndex++)
        {
            returnValue[returnValueIndex] = sPos[i];
        }

        return returnValue;
    }
    public Vector3[] ColumnFormation(Vector3 pos, Vector3 frontDir, int nFollowers, int nSupportFollowers, float minH)
    {
        Vector3[] fPos = new Vector3[nFollowers+nSupportFollowers];
        Vector3 sidePos = pos;
        Vector3 stepDir = Quaternion.Euler(0, 180, 0) * frontDir.normalized;

        float step = formationTightness;

        for(int i = 0; i < (nFollowers+nSupportFollowers); i++)
        {
            sidePos = sidePos + (stepDir * step);
            sidePos.y = 0;
            Collider[] co = Physics.OverlapBox(sidePos, new Vector3(0.49f, 0.49f, 0.49f), Quaternion.LookRotation(frontDir, Vector3.up), LayerMask.GetMask("Building"));

            int xPos = (int)Mathf.Round(sidePos.x);
            int zPos = (int)Mathf.Round(sidePos.z);

            if(co.Length == 0 && xPos >= 0 && xPos <= mapSize && zPos >= 0 && zPos <= mapSize && MapMinHeight(xPos, zPos) <= minH)
            {
                fPos[i] = sidePos;
            }
            else
            {
                fPos[i] = new Vector3(-1f, -1f, -1f);
            }
        }

        return fPos;
    }

    public GameObject DrawSquare(float halfExtend, float height, Color c, Vector3 pos)
    {
        if(squarePrefab != null)
        {
            GameObject o = Instantiate(squarePrefab);
            o.transform.localScale = new Vector3(halfExtend, height, halfExtend);
            o.transform.position = pos;
            Renderer m = o.GetComponent<Renderer>();
            m.material = new Material(Shader.Find("Standard"));
            m.material.color = c;
            return o;
        }
        else Debug.Log("squarePrefab == null");

        return null;
    }

    public GameObject DrawBall(float scale, Vector3 pos)
    {
        if(ballPrefab != null)
        {
            GameObject o = Instantiate(ballPrefab);
            o.transform.localScale = new Vector3(2 * scale, 2 * scale, 2 * scale);
            o.transform.position = pos;
            return o;
        }
        else Debug.Log("ballPrefab == null");

        return null;
    }
    public GameObject DrawCilinder(float scaleXZ, float scaleY, Vector3 pos)
    {
        if(cilinderPrefab != null)
        {
            GameObject o = Instantiate(cilinderPrefab);
            o.transform.localScale = new Vector3(2 * scaleXZ, 2 * scaleY, 2 * scaleXZ);
            o.transform.position = pos;
            return o;
        }
        else Debug.Log("cilinderPrefab == null");

        return null;
    }
    public GameObject DrawLine(Vector3 start,Vector3 end,Color color, float destroyT)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.05f;
        lr.endWidth = 0.05f;
        lr.SetPosition(0,start);
        lr.SetPosition(1,end);
        if(destroyT != 0) Destroy(myLine, destroyT);
        return myLine;
    }
    public GameObject DrawLine(Vector3 start, Vector3 dir, float dist, Color color, float destroyT)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.5f;
        lr.endWidth = 0.5f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, start + (dir * dist));
        if(destroyT != 0) Destroy(myLine, destroyT);
        return myLine;
    }
}
