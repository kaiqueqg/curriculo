﻿using UnityEngine;

public class Noise
{
    public enum NoiseType { Footstep, Wind };
    public NoiseType type;
    public Vector3 pos;
}
