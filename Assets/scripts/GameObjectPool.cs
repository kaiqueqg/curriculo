﻿using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    private GameObject myType;
    private List<GameObject> _list;
    private int _defaultSize = 10;
    private int _expandLeap = 10;
    private int amountAvailable;

    private GameObjectPool(){}

    public GameObjectPool(GameObject t)
    {
        float time = Time.deltaTime;
        myType = t;
        _list = new List<GameObject>();
        for (int i = 0; i < _defaultSize; i++)
        {
            _list.Add(Instantiate(myType));
            _list[i].SetActive(false);
        }
        amountAvailable = _defaultSize;

        time -= Time.deltaTime;
    }

    public GameObjectPool(GameObject t, int startSize)
    {
        float time = Time.deltaTime;

        myType = t;
        _list = new List<GameObject>();
        for (int i = 0; i < startSize; i++)
        {
            _list.Add(Instantiate(myType));
            _list[i].SetActive(false);
        }
        amountAvailable = startSize;

        time -= Time.deltaTime;
        Debug.Log("Pool created: " + time);
    }

    public void Expand()
    {
        GameObject obj;
        for (int i = 0; i < _expandLeap; i++)
        {
            obj = Instantiate(myType);
            obj.SetActive(false);
            _list.Add(obj);
        }
        Debug.Log(amountAvailable);
        amountAvailable += _expandLeap;
    }

    public void Expand(int count)
    {
        GameObject obj;
        for(int i = 0; i < count; i++)
        {
            obj = Instantiate(myType);
            obj.SetActive(false);
            _list.Add(obj);
        }
        Debug.Log(amountAvailable);
        amountAvailable += count;
    }

    public GameObject GetObject()
    {
        for(int i = 0; i < _list.Count; i++)
        {
            if (!_list[i].activeInHierarchy)
            {
                _list[i].SetActive(true);
                return _list[i];
            }
        }

        Expand(1);
        _list[_list.Count - 1].SetActive(true);
        return _list[_list.Count-1];
    }

    public void ReturnObject(GameObject obj)
    {
        amountAvailable--;
        obj.SetActive(false);
        Debug.Log(amountAvailable);
    }
}
