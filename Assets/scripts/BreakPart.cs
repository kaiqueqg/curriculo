﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakPart : MonoBehaviour
{
    BaseDestructibleObj o;

    private void Start()
    {
        o = transform.root.GetComponent<BaseDestructibleObj>();
    }

    void OnParticleCollision(GameObject other)
    {
        //Debug.Log("Hit Collision " + name);
        if(o != null) o.Hit(10f);
        else Debug.Log("No BaseDestructibleObj on " + name);
    }
}
