﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public abstract class UnitObj : BaseDestructibleObj
{

    public enum UnitType { Land, Air };
    public enum DefendStyle { Line, Arrow };
    public enum FormationStyle { Line, Arrow, Column };
    public enum ROE { DontAttack, AlwaysAttack };
    public enum AnimationState { Stopped, Acc, Moving, Braking };
    public enum SoundState { Waiting, Acc, Moving, Braking };

    public SoundState soundState;
    public AnimationState animationState;

    //Particles
    public ParticleSystem exhaustParticles;

    protected AudioSource waitingSound;
    protected AudioSource accSound;
    protected AudioSource movingSound;
    protected AudioSource brakingSound;

    public UnitType unitType;
    public FormationStyle moveStyle;
    public ROE roe;

    //Generic Stuffs
    public int index;
    public GameObject defenceGhostPrefab;
    public GameObject defenceRangePrefab;

    //Prefabs
    public Transform raycastPoint;
    public GameObject turretPrefab;
    public GameObject[] prefabs;
    public RuntimeAnimatorController[] animators;
    public Animator anim;

    //Formation Lider
    public GameObject isLiderIndicator;
    public bool isLider = false;
    public UnitObj myLider;
    public List<UnitObj> followers;
    public List<UnitObj> supportFollowers;

    //Formation Follower
    public bool movingInFormation = false;
    public UnitObj movLider;
    public float relativeRotation;
    public bool inFormation;

    //Waypoints
    protected AStar aStar;
    public float minHeight;
    public List<Vector3> waypoints;
    public int wPos;
    public Vector3 defencePos;
    public float movementWaypointAcceptableDistance;
    public float formationAcceptableDistance;

    //PID
    public PIDControl movementControl;
    public PIDControl turnControl;

    //Movement
    public Animator moveAnim;
    public bool move = false;
    protected float maxVelocity;
    protected float formationVelocity;
    public float desiredVelocity;
    public float currentVelocity;
    protected float minVelocity;
    protected float cutoffVelocity;
    public bool avoidingUnits = false;

    //Rates
    protected float maxAcceRate;
    protected float maxBrakeRate;
    protected float cutoffAcceBrakeRate;

    //Rotation
    protected Vector3 lastDirection;

    public Vector3 desiredRotation;
    public float currentRotationSpeed;
    protected float maxRotationSpeed;
    protected float rotationRate;
    public float angleBetween;
    protected float aceptableAngle;

    //Enemies
    protected List<LandUnit> enemies;
    public BaseDestructibleObj enemyInFocus;
    public string enemyLayer = "";
    
    //Search
    public bool attack;
    public float searchTime;
    public float lastSearchTime;

    //Gun
    public float attackDamage;
    public ParticleSystem[] muzzleFlash;
    protected float reloadTime;
    protected float lastShot;
    protected float turretTurnRate;
    protected float turretMaxAngle;
    public Animator fireAnim;
    
    //Range
    public float minDefenceRange;
    public float attackRange;
    public float sightRange;

    private float defenceRange;
    public void SetDefenceRange(float v) { defenceRange = Mathf.Clamp(v, minDefenceRange, sightRange); }
    public float GetDefenceRange() { return defenceRange; }

    public override void Awake()
    {
        base.Awake();
        destructableType = DestructableType.Unit;
        aStar = new AStar();
        lastDirection = Vector3.forward;
    }

    public void Start()
    {
        if(index < player.ghostUnits.Length)
        {
            defenceGhostPrefab = player.GetInst(player.ghostUnits[index]);
            defenceGhostPrefab.SetActive(false);
            defenceRangePrefab = player.GetInst(player.cilinderPrefab);
            defenceRangePrefab.SetActive(false);
        }
    }

    public override void Update()
    {
        //temp
        if(isLiderIndicator != null) isLiderIndicator.SetActive(isLider);

        base.Update();
        MoveAndTurn();
        MovementAnimation();
        Combat();
        Sound();
    }

    protected virtual void MovementAnimation()
    {
        if(moveAnim != null)
        {
            switch(animationState)
            {
                case AnimationState.Stopped:
                    moveAnim.SetTrigger("Stop");
                    if(currentVelocity != 0) animationState = AnimationState.Acc;
                    break;
                case AnimationState.Acc:
                    moveAnim.SetTrigger("Acce");
                    if(currentVelocity != 0)animationState = AnimationState.Moving;
                    else animationState = AnimationState.Braking;
                    break;
                case AnimationState.Braking:
                    moveAnim.SetTrigger("Brake");
                    if(currentVelocity == 0) animationState = AnimationState.Stopped;
                    else animationState = AnimationState.Acc;
                    break;
                case AnimationState.Moving:
                    moveAnim.SetTrigger("Move");
                    if(currentVelocity == 0) animationState = AnimationState.Braking;
                    break;
            }
        }
    }

    public virtual void Sound()
    {

    }

    public virtual void Combat()
    {
        lastShot += Time.deltaTime;

        if(roe == ROE.AlwaysAttack)
        {
            SearchEnemy();
            if(enemyInFocus != null)
            {
                Vector3 enemyPos = new Vector3(enemyInFocus.transform.position.x, turretPrefab.transform.position.y, enemyInFocus.transform.position.z);
                Quaternion rotationTowardEnemy = Quaternion.LookRotation((enemyPos - turretPrefab.transform.position).normalized);
                turretPrefab.transform.rotation = Quaternion.RotateTowards(turretPrefab.transform.rotation, rotationTowardEnemy, turretTurnRate * Time.deltaTime);
                

                if(Quaternion.Angle(turretPrefab.transform.rotation, rotationTowardEnemy) < 0.1f && Quaternion.Angle(transform.rotation, rotationTowardEnemy) < turretMaxAngle)
                {
                    Shot(enemyInFocus);
                }
            }
            else
            {
                turretPrefab.transform.rotation = Quaternion.RotateTowards(turretPrefab.transform.rotation, transform.rotation, turretTurnRate*Time.deltaTime);
            }
        }
    }
    
    public virtual void MoveDirectToPoint(Vector3 pos)
    {
        move = false;
        waypoints = new List<Vector3>();
        waypoints.Add(pos);
        wPos = 0;
        move = true;
    }
    

    private void MoveTo(Vector3 pos)
    {
        if(waypoints != null && waypoints.Count > 0 && waypoints[waypoints.Count - 1] == pos) return;

        waypoints = aStar.FindPath(transform.position, pos, player.mapPrecision, true, minHeight, player.map, LayerMask.GetMask("Building"));
        wPos = 0;
    }

    public virtual void MoveToPoint(Vector3 pos)
    {
        MoveTo(pos);

        if(waypoints != null && waypoints.Count > 0)
        {
            if(waypoints.Count > 1) // Good direction from penultimate to last
            {
                lastDirection = waypoints[waypoints.Count - 1] - waypoints[waypoints.Count - 2];
            }
            else
            {
                if(waypoints[0] != transform.position) // To avoid 0,0,0 direction
                {
                    lastDirection = waypoints[0] - transform.position;
                }
                else
                {
                    lastDirection = transform.rotation.eulerAngles;
                }
            }
        }

        move = true;
    }

    public virtual void MoveToPoint(Vector3 pos, Vector3 lastDir)
    {
        MoveTo(pos);
        move = true;
        lastDirection = lastDir;
    }

    public void MoveAsLider(List<UnitObj> f, List<UnitObj> sF, Vector3 dPos)
    {
        if(myLider != null) myLider.RemoveFromFormation(this);
        myLider = null;

        movingInFormation = true;
        isLider = true;
        followers = f;
        supportFollowers = sF;

        for(int i = 0; i < f.Count; i++)
        {
            f[i].SetAsFollower(this);
        }

        for(int i = 0; i < sF.Count; i++)
        {
            sF[i].SetAsFollower(this);
        }

        Destroy(Instantiate(player.pointMesh, dPos, Quaternion.identity), 1f);

        MoveToPoint(dPos);
    }

    protected List<UnitObj> OrganizedUnitsByClosestToPoint(List<UnitObj> units, Vector3[] points)
    {
        if(units.Count > 0)
        {
            List<UnitObj> rtnList = new List<UnitObj>();

            for(int i = 0; i < points.GetLength(0); i++)
            {
                UnitObj current = units[0];
                float dist = Vector3.Distance(current.gameObject.transform.position, points[i]);
                float currentDist = Mathf.Infinity;

                for(int j = 1; j < units.Count; j++)
                {
                    currentDist = Vector3.Distance(units[j].gameObject.transform.position, points[i]);

                    if(currentDist < dist)
                    {
                        current = units[j];
                        dist = currentDist;
                    }
                }

                rtnList.Add(current);
                units.Remove(current);
            }

            return rtnList;
        }

        return units;
    }
    
    public void RemoveFromFormation(UnitObj objToRemove)
    {
        followers.Remove(objToRemove);
        supportFollowers.Remove(objToRemove);
    }

    protected void Shot(BaseDestructibleObj enemy)
    {
        if(enemy == null)
        {
            Debug.Log("Shot enemy null");
        }
        else
        {
            if(turretPrefab != null && lastShot >= reloadTime)
            {
                lastShot = 0;
                
                for(int i = 0; i < muzzleFlash.Length; i++) muzzleFlash[i].Play();

                if(sounds != null && sounds.Length > 0) sounds[0].Play();
            }
            else
            {
                //Debug.Log("Reloading...Shooting in: " + (reloadTime - lastShot) + "s");
            }
        }
    }
    
    public void SetAsFollower(UnitObj lider)
    {
        myLider = lider;
        movingInFormation = true;
        move = false;
        isLider = false;
    }
    

    //Defence stuff
    public void ShowDefencePos(bool v)
    {
        defenceGhostPrefab.SetActive(v);
        defenceRangePrefab.SetActive(v);
    }
    public void UpdateDefencePos(Vector3 ghostDir)
    {
        defenceGhostPrefab.transform.position = defencePos;
        defenceGhostPrefab.transform.rotation = Quaternion.LookRotation(ghostDir);
        defenceRangePrefab.transform.position = defencePos;
        defenceRangePrefab.transform.localScale = new Vector3(2*defenceRange, minHeight, 2*defenceRange);
    }
    public void SetDefence(Vector3 pos)
    {
        defenceGhostPrefab.transform.position = pos;
        defenceRangePrefab.transform.position = pos;
        defenceRangePrefab.transform.localScale = new Vector3(2*defenceRange, minHeight, 2*defenceRange);
        defencePos = pos;
    }
    
    public virtual void Stop()
    {
        waypoints = new List<Vector3>();
        wPos = 1;
    }
    public abstract void MoveAndTurn();
    
    public virtual void SearchEnemy()
    {
        RaycastHit h;
        LandUnit u;
        LandUnit tempEnemyInFocus = null;

        float closestDist = GetDefenceRange();
        List<LandUnit> tempEnemies = new List<LandUnit>();
        Collider[] co = Physics.OverlapCapsule(transform.position, new Vector3(transform.position.x, minHeight, transform.position.z), GetDefenceRange(), LayerMask.GetMask(enemyLayer));

        for(int i = 0; i < co.Length; i++)
        {
            if(Physics.Raycast(raycastPoint.position, co[i].transform.position - raycastPoint.position, out h/*, Mathf.Infinity, LayerMask.GetMask(enemyLayer)*/))
            {
                u = h.collider.GetComponent<LandUnit>();
                if(u != null && u.side != side)
                {
                    float dist = Vector3.Distance(transform.position, co[i].transform.position);
                    if(dist <= closestDist)
                    {
                        closestDist = dist;
                        tempEnemyInFocus = u;
                    }

                    tempEnemies.Add(u);
                }
            }
        }

        if(tempEnemies.Count > 0) enemies = tempEnemies;
        enemyInFocus = tempEnemyInFocus;
    }

    public override void ObjectSelected(GameObject o){}
    public override void ButtonSelected(int index)
    {
        switch(index)
        {
            case 1:
                roe = player.roe;
                break;
            case 2:
                moveStyle = player.moveStyle;
                break;
        }
    }
}
