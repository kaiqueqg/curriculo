﻿using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Tank : TrackLandUnit
{
    public static int cost = -2;
    public static int lostCost = -1;

    public override void Awake()
    {
        base.Awake();

        //Type
        moveStyle = FormationStyle.Arrow;
        //roe = ROE.OnlyDefend;
        myTrackUnitType = TrackUnitType.Tank;

        //Map
        minHeight = 0.5f;
        
        //Movement
        maxVelocity = 15f;
        formationVelocity = 15f;
        desiredVelocity = 15f;
        minVelocity = -0.03f;
        cutoffVelocity = 0f;
        maxAcceRate = 10f;
        maxBrakeRate = -10f;
        cutoffAcceBrakeRate = 0.0f;
        movementWaypointAcceptableDistance = 1f;
        formationAcceptableDistance = 5f;

        //Turning
        maxRotationSpeed = 5f;
        rotationRate = 3f;
        aceptableAngle = 2f;

        //PIDControl
        movementControl = new PIDControl(1f, 0f, 0f, maxBrakeRate, maxAcceRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        movementControl.debug = false;
        turnControl = new PIDControl(1f, 0f, 0f, -rotationRate, rotationRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        turnControl.debug = false;

        //Colliders
        enemyLayer = "Enemy";

        //Combat
        maxHealth = 50;
        health = 50;
        minDefenceRange = 200f;
        sightRange = 200f;
        SetDefenceRange(200f);
        attackDamage = 0;
        attackRange = 300f;
        searchTime = 1f;
        reloadTime = 4f;
        lastShot = 5f;
        turretTurnRate = 40f;
        turretMaxAngle = 181f;

        index = 0;
        lostCost = 0;
        buyCost = 1;

        //Sound
        waitingSound = sounds[1];
        accSound = sounds[2];
        movingSound = sounds[3];
        brakingSound = sounds[4];
        soundState = SoundState.Waiting;

        //Side
        side = 1;

        //UI
        selectedBoxPrefab = gameObject.transform.GetChild(1).gameObject;
    }

    protected override void HitSound()
    {
        sounds[5].Play();
    }

    public override void Sound()
    {
        if(sounds.Length > 0)
        {
            if(currentVelocity == 0 && soundState != SoundState.Waiting && soundState != SoundState.Braking)
            {
                accSound.Stop();
                waitingSound.Stop();
                movingSound.Stop();

                if(!brakingSound.isPlaying) brakingSound.Play();
                soundState = SoundState.Braking;
                //Debug.Log("Braking");
            }
            else if(currentVelocity == 0 && !brakingSound.isPlaying &&soundState == SoundState.Braking)
            {
                accSound.Stop();
                brakingSound.Stop();
                movingSound.Stop();

                if(!waitingSound.isPlaying) waitingSound.Play();
                soundState = SoundState.Waiting;
                //Debug.Log("Waiting");
            }
            else if(currentVelocity != 0 && soundState != SoundState.Acc && soundState != SoundState.Moving)
            {
                waitingSound.Stop();
                brakingSound.Stop();
                movingSound.Stop();

                if(!accSound.isPlaying) accSound.Play();
                soundState = SoundState.Acc;
                //Debug.Log("Acc");
            }
            else if(!accSound.isPlaying && currentVelocity != 0)
            {
                accSound.Stop();
                brakingSound.Stop();
                accSound.Stop();

                if(!movingSound.isPlaying) movingSound.Play();
                soundState = SoundState.Moving;
                //Debug.Log("Moving");
            }
        }
    }

    public override void AddMenuChoises(StrategyPlayerControl p)
    {

    }
}
