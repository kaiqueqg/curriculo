﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TrackLandUnit : LandUnit
{
    public enum TrackUnitType { Tank }
    public TrackUnitType myTrackUnitType;

    //temp
    public bool debugControl;

    public override void Awake()
    {
        base.Awake();

        myLandUnitType = LandUnitType.Track;
    }

    public override void AddMenuChoises(StrategyPlayerControl p)
    {
        throw new NotImplementedException();
    }
    
    public override void MoveAndTurn()
    {
        movementControl.debug = debugControl;

        if(move && waypoints != null && waypoints.Count > 0)
        {
            if(wPos < waypoints.Count && Vector3.Distance(waypoints[wPos], transform.position) < movementWaypointAcceptableDistance) // got there
            {
                wPos++;
            }

            Rotate();

            bool keepGoing = true;
            if(isLider)
            {
                movementControl.SetkI(0f);
                Vector3[] fPos = new Vector3[0];
                
                switch(moveStyle)
                {
                    case FormationStyle.Column:
                        fPos = player.ColumnFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                    case FormationStyle.Arrow:
                        fPos = player.ArrowFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                    case FormationStyle.Line:
                        fPos = player.LineFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                }

                Vector3 dir;

                //Deciding last direction
                if(wPos >= waypoints.Count) //last direction
                {
                    dir = lastDirection;
                }
                else if(waypoints[wPos] == transform.position) // avoiding vector 0,0,0
                {
                    dir = transform.forward;
                }
                else //valid direction
                {
                    dir = waypoints[wPos] - transform.position;
                }

                //Ordering followers to move
                if(followers.Count > 0) followers = OrganizedUnitsByClosestToPoint(followers, fPos.GetSlice<Vector3>(0, followers.Count));

                int fPosIndex = 0;
                for(int i = 0; i < followers.Count; i++, fPosIndex++)
                {
                    if(fPos[fPosIndex] != new Vector3(-1, -1 , -1))
                    {
                        followers[i].MoveToPoint(fPos[fPosIndex], dir);
                    }
                    else
                    {
                        followers[i].MoveToPoint(followers[i].transform.position, dir);
                    }
                }
                supportFollowers = OrganizedUnitsByClosestToPoint(supportFollowers, fPos.GetSlice<Vector3>(fPosIndex));
                for(int i = 0; i < supportFollowers.Count; i++, fPosIndex++)
                {
                    if(fPos[fPosIndex] != new Vector3(-1, -1, -1))
                    {
                        supportFollowers[i].MoveToPoint(fPos[fPosIndex], dir);
                    }
                    else
                    {
                        supportFollowers[i].MoveToPoint(supportFollowers[i].transform.position, dir);
                    }
                }

                //Checking if they are on point
                for(int i = 0; i < followers.Count && keepGoing; i++)
                {
                    if(followers[i] != null)
                    {
                        keepGoing = followers[i].inFormation;
                    }
                }
                for(int i = 0; i < supportFollowers.Count && keepGoing; i++)
                {
                    if(supportFollowers[i] != null)
                    {
                        keepGoing = supportFollowers[i].inFormation;
                    }
                }
            }
            else
            {
                movementControl.SetkI(0.01f);
            }
            
            // lider tested if everyone was in formation, keepGoing of followers are always set to true
            if(keepGoing) 
            {
                if(wPos < waypoints.Count)
                {
                    if(angleBetween.IsBetween(-2f, 2f))
                    {
                        currentVelocity += maxAcceRate * Time.deltaTime;
                        if(currentVelocity > maxVelocity) currentVelocity = maxVelocity;
                        transform.position = Vector3.MoveTowards(transform.position, waypoints[wPos], currentVelocity * Time.deltaTime);
                    }
                }
                else
                {
                    currentVelocity = 0;
                }
            }

            if(Vector3.Distance(waypoints[waypoints.Count - 1], transform.position) < formationAcceptableDistance)
            {
                inFormation = true;
            }
            else inFormation = false;
        }
        else Brake();
    }
    
    protected void Rotate()
    {
        if(waypoints != null && waypoints.Count > 0)
        {
            //float power;
            Vector3 dir;

            //Deciding last direction
            if(wPos >= waypoints.Count) //last direction
            {
                dir = lastDirection;
            }
            else if(waypoints[wPos] == transform.position) // avoiding vector 0,0,0
            {
                dir = transform.forward;
            }
            else //valid direction
            {
                dir = waypoints[wPos] - transform.position;
            }
            angleBetween = Vector3.Angle(dir, transform.forward);
            transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, dir, 2f*Time.deltaTime, 0.0f));
        }
    }

    public void Brake()
    {
        currentVelocity += (maxBrakeRate * Time.deltaTime);
        if(currentVelocity < 0) currentVelocity = 0;
    }
}
