﻿using UnityEngine;
using System.Collections;
using System;

public class WheelLandUnit : LandUnit
{
    public enum WheelLandUnitType { Construction, OffRoad };
    public WheelLandUnitType myWheelLandUnitType;

    public GameObject leftWheel;
    public GameObject rightWheel;
    public GameObject[] backWheel;

    protected bool onBuildingMode;

    //temp debug
    GameObject point;
    GameObject circle;

    public override void Awake()
    {
        base.Awake();

        onBuildingMode = true;
        myLandUnitType = LandUnitType.Wheel;

        //temp debug
        //point = player.DrawBall(0.1f, transform.position);
        //point.name = "tanPoint";
        //circle = player.DrawBall(0.1f, transform.position);
        //circle.name = "circle";
    }

    public override void MoveAndTurn()
    {
        if(!onBuildingMode && move && waypoints != null && waypoints.Count > 0)
        {
            if(wPos < waypoints.Count && Vector3.Distance(waypoints[wPos], transform.position) < movementWaypointAcceptableDistance) // got there
            {
                wPos++;
            }
            
            bool keepGoing = true;
            if(isLider)
            {
                movementControl.SetkI(0f);
                Vector3[] fPos = new Vector3[0];

                switch(moveStyle)
                {
                    case FormationStyle.Column:
                        fPos = player.ColumnFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                    case FormationStyle.Arrow:
                        fPos = player.ArrowFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                    case FormationStyle.Line:
                        fPos = player.LineFormation(transform.position, transform.forward, followers.Count, supportFollowers.Count, minHeight);
                        break;
                }

                Vector3 dir;

                //Deciding last direction
                if(wPos >= waypoints.Count) //last direction
                {
                    dir = lastDirection;
                }
                else if(waypoints[wPos] == transform.position) // avoiding vector 0,0,0
                {
                    dir = transform.forward;
                }
                else //valid direction
                {
                    dir = waypoints[wPos] - transform.position;
                }
                
                int fPosIndex = 0;
                for(int i = 0; i < followers.Count; i++, fPosIndex++)
                {
                    followers[i].MoveToPoint(fPos[fPosIndex], dir);
                }
                for(int i = 0; i < supportFollowers.Count; i++, fPosIndex++)
                {
                    supportFollowers[i].MoveToPoint(fPos[fPosIndex], dir);
                }

                for(int i = 0; i < followers.Count && keepGoing; i++)
                {
                    if(followers[i] != null)
                    {
                        keepGoing = followers[i].inFormation;
                    }
                }
                for(int i = 0; i < supportFollowers.Count && keepGoing; i++)
                {
                    if(supportFollowers[i] != null)
                    {
                        keepGoing = supportFollowers[i].inFormation;
                    }
                }
            }
             else
            {
                movementControl.SetkI(0.01f);
            }

            // lider tested if everyone was in formation, keepGoing of followers are always set to true
            if(keepGoing)
            {
                if(wPos < waypoints.Count)
                {
                    //
                    Vector3 correctWaypoint = waypoints[wPos];

                    if(movingInFormation)
                    {

                        if(Vector3.Distance(transform.position, waypoints[wPos]) > 10f)
                        {
                            //Debug.Log("onCircle " + Vector3.Distance(waypoints[wPos], transform.position));
                            Vector2 dir = new Vector2(transform.position.x, transform.position.z) - new Vector2(waypoints[wPos].x, waypoints[wPos].z);
                            dir.Normalize();
                            float cross = Vector3.Cross(dir, new Vector2(lastDirection.x, lastDirection.z)).z;
                            
                            //waypoint is on the right or on the left?
                            if(cross > 0)
                            {
                                Vector3 right = Quaternion.Euler(0, 90, 0) * lastDirection.normalized;
                                right = right.normalized * 5f;
                                Vector3 circlePoint = waypoints[wPos] + right;
                                Vector3 direcFromUnitToCircle = transform.forward.normalized;
                                Vector3 directionToTangencialPoint = Quaternion.Euler(0, -90, 0) * direcFromUnitToCircle.normalized;
                                Vector3 tanPoint = circlePoint + (directionToTangencialPoint.normalized * (maxVelocity / 3.14f));
                                correctWaypoint = tanPoint;

                                //player.DrawLine(waypoints[wPos], lastDirection, 1f, Color.red, 0.1f);
                                //player.DrawLine(waypoints[wPos], right, 10f, Color.white, 0.1f);
                                //circle.transform.position = circlePoint;
                                //player.DrawLine(transform.position, direcFromUnitToCircle, 100f, Color.black, 0.1f);
                                //player.DrawLine(circlePoint, directionToTangencialPoint, 100f, Color.green, 0.1f);
                                //point.transform.position = tanPoint;
                                //player.DrawLine(transform.position, (tanPoint - transform.position).normalized, 100f, Color.blue, 0.1f);
                            }
                            else
                            {
                                Vector3 left = Quaternion.Euler(0, -90, 0) * lastDirection.normalized;
                                left = left.normalized * 5f;
                                Vector3 circlePoint = waypoints[wPos] + left;
                                Vector3 direcFromUnitToCircle = transform.forward.normalized;
                                Vector3 directionToTangencialPoint = Quaternion.Euler(0, 90, 0) * direcFromUnitToCircle.normalized;
                                Vector3 tanPoint = circlePoint + (directionToTangencialPoint.normalized * (maxVelocity / 3.14f));
                                correctWaypoint = tanPoint;

                                //player.DrawLine(waypoints[wPos], lastDirection, 1f, Color.red, 0.1f);
                                //player.DrawLine(waypoints[wPos], left, 10f, Color.white, 0.1f);
                                //circle.transform.position = circlePoint;
                                //player.DrawLine(transform.position, direcFromUnitToCircle, 100f, Color.black, 0.1f);
                                //player.DrawLine(circlePoint, directionToTangencialPoint, 100f, Color.green, 0.1f);
                                //point.transform.position = tanPoint;
                                //player.DrawLine(transform.position, (tanPoint - transform.position).normalized, 100f, Color.blue, 0.1f);
                            }
                        }
                    }

                    currentVelocity += maxAcceRate * Time.deltaTime;
                    if(currentVelocity > maxVelocity) currentVelocity = maxVelocity;
                    transform.Translate(Vector3.forward * currentVelocity * Time.deltaTime);

                    //Rotate
                    Quaternion rotationTowardEnemy = Quaternion.LookRotation((correctWaypoint - transform.position).normalized);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, rotationTowardEnemy, Time.deltaTime * currentVelocity.Map(0, maxVelocity, 0, maxRotationSpeed));

                    //WheelRotation
                    float angle = Vector3.Angle(transform.forward, correctWaypoint - transform.position);
                    float crossProduct = (Vector3.Cross(correctWaypoint - transform.position, transform.forward)).y;
                    if(angle <= 30f && angle > 1f)
                    {
                        //Debug.Log("f");
                        rightWheel.transform.rotation = Quaternion.LookRotation(Vector3.Slerp(rightWheel.transform.forward, correctWaypoint - rightWheel.transform.position, Time.deltaTime));
                        leftWheel.transform.rotation = Quaternion.LookRotation(Vector3.Slerp(leftWheel.transform.forward, correctWaypoint - leftWheel.transform.position, Time.deltaTime));
                    }
                    else if(angle > 30f)
                    {
                        //Debug.Log("m");
                        if(crossProduct > 0)
                        {
                            rightWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 60, 0);
                            leftWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 60, 0);
                        }
                        else
                        {
                            rightWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 120, 0);
                            leftWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 120, 0);
                        }
                    }
                    else
                    {
                        //Debug.Log("x");
                        rightWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 90, 0);
                        leftWheel.transform.rotation = transform.rotation * Quaternion.Euler(0, 90, 0);
                    }
                }
                else
                {
                    currentVelocity = 0;
                }
            }

            if(Vector3.Distance(waypoints[waypoints.Count - 1], transform.position) < formationAcceptableDistance)
            {
                inFormation = true;
            }
            else inFormation = false;
        }
        else Brake();
    }

    public void Brake()
    {
        if(currentVelocity != 0)
        {
            currentVelocity += (maxBrakeRate * Time.deltaTime);
            if(currentVelocity < 0) currentVelocity = 0;
        }
    }
    public override void AddMenuChoises(StrategyPlayerControl p){}
}
