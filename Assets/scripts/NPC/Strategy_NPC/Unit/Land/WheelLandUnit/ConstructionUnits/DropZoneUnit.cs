﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class DropZoneUnit : BaseConstructionUnit
{
    protected float timeDeploy = 0f;
    protected float truckTurnAngle;
    protected float truckMovementAcceptableDistance;

    public GameObject DropzoneUnitAirdrop;
    public GameObject TankAirdrop;

    public override void Awake()
    {
        base.Awake();

        lostCost = 2;
        buyCost = 1;

        //Side
        index = 1;

        //UI
        //selectedBoxPrefab = gameObject.transform.GetChild(1).gameObject;

        //Type
        moveStyle = FormationStyle.Column;
        roe = ROE.AlwaysAttack;
        myConstructionWheelLandUnitType = ConstructionWheelLandUnitType.DropZone;

        //Map
        minHeight = 0.5f;

        //Movement
        maxVelocity = 15f;
        formationVelocity = 15f;
        desiredVelocity = 15f;
        minVelocity = -0.5f;
        cutoffVelocity = 0f;
        maxAcceRate = 10f;
        maxBrakeRate = -10f;
        cutoffAcceBrakeRate = 0.0f;
        movementWaypointAcceptableDistance = 1f;
        formationAcceptableDistance = 10f;

        //Turning
        maxRotationSpeed = 180f;
        rotationRate = 5f;
        aceptableAngle = 2f;
        truckTurnAngle = 20f;

        //PIDControl
        movementControl = new PIDControl(0.5f, 0.03f, 0.01f, maxBrakeRate, maxAcceRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        movementControl.debug = false;
        turnControl = new PIDControl(0.5f, 0.03f, 0f, -rotationRate, rotationRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        turnControl.debug = false;

        //Combat
        maxHealth = 10;
        health = 10;
        minDefenceRange = 0.1f;
        SetDefenceRange(20f);
        sightRange = 20f;
        attackDamage = 30;
        attackRange = 10f;
        searchTime = 1f;
        reloadTime = 4f;
        lastShot = 5f;
        
        //Sound
        waitingSound = sounds[0];
        accSound = sounds[1];
        movingSound = sounds[2];
        brakingSound = sounds[3];
        soundState = SoundState.Waiting;

        //Side
        side = 1;

        //UI
        //selectedBoxPrefab = gameObject.transform.GetChild(1).gameObject;

        //InvokeRepeating("FindEnemies", 0f, searchTime);
    }

    public override void Update()
    {
        base.Update();

        if(buildingInprogress)
        {
            //Debug.Log("dropZone true");
            buildingPrefab.SetActive(true);
            onBuildingMode = true;
            //anim.SetBool("Open", true);
        }
        else
        {
            //Debug.Log("dropZone false");
            buildingPrefab.SetActive(false);
            //onBuildingMode = false;
            //anim.SetBool("Open", false);
        }
    }

    public override void Sound()
    {
        if(sounds.Length > 0)
        {
            if(currentVelocity == 0 && soundState != SoundState.Waiting && soundState != SoundState.Braking)
            {
                accSound.Stop();
                waitingSound.Stop();
                movingSound.Stop();

                if(!brakingSound.isPlaying) brakingSound.Play();
                soundState = SoundState.Braking;
                //Debug.Log("Braking");
            }
            else if(currentVelocity == 0 && !brakingSound.isPlaying && soundState == SoundState.Braking)
            {
                accSound.Stop();
                brakingSound.Stop();
                movingSound.Stop();

                if(!waitingSound.isPlaying) waitingSound.Play();
                soundState = SoundState.Waiting;
                //Debug.Log("Waiting");
            }
            else if(currentVelocity != 0 && soundState != SoundState.Acc && soundState != SoundState.Moving)
            {
                waitingSound.Stop();
                brakingSound.Stop();
                movingSound.Stop();

                if(!accSound.isPlaying)
                {
                    accSound.Play();
                    if(exhaustParticles != null) exhaustParticles.Play();
                }
                soundState = SoundState.Acc;
                //Debug.Log("Acc");
            }
            else if(!accSound.isPlaying && currentVelocity != 0)
            {
                accSound.Stop();
                brakingSound.Stop();
                accSound.Stop();

                if(!movingSound.isPlaying) movingSound.Play();
                soundState = SoundState.Moving;
                //Debug.Log("Moving");
            }
        }
    }

    protected override void HitSound()
    {
        
    }

    public override void ButtonSelected(int index)
    {
        switch(index)
        {
            case 4:
                buildingInprogress = !buildingInprogress;
                //anim.SetBool("Open", buildingPrefab);
                //Debug.Log("Set building in progress to " + buildingInprogress);
                player.dropzoneOptions[0].gameObject.SetActive(true);
                player.dropzoneOptions[1].gameObject.SetActive(true);
                break;
            case 5:
                if(player.GetPoints() >= 1)
                {
                    player.PointsUpdate(-1);
                    player.tankAvailable++;
                    Destroy(Instantiate(TankAirdrop, buildingPrefab.transform.position, transform.rotation), 10f);
                    Invoke("SpawnTank", 10f);
                }
                else
                {
                    Debug.Log("Not enough sucess!");
                }
                break;
            case 6:
                if(player.GetPoints() >= 1)
                {
                    player.PointsUpdate(-1);
                    Destroy(Instantiate(DropzoneUnitAirdrop, buildingPrefab.transform.position, transform.rotation), 10f);
                    Invoke("SpawnDropZoneUnit", 10f);
                }
                else
                {
                    Debug.Log("Not enough sucess!");
                }
                break;
        }
        
    }

    private void SpawnTank()
    {
        GameObject spawnVehicle = Instantiate(player.units[0], buildingPrefab.transform.position, transform.rotation);

        UnitObj u = spawnVehicle.GetComponent<UnitObj>();
        u.GetComponent<UnitObj>().player = player;
    }

    private void SpawnDropZoneUnit()
    {
        GameObject spawnVehicle = Instantiate(player.units[1], buildingPrefab.transform.position, transform.rotation);

        UnitObj u = spawnVehicle.GetComponent<UnitObj>();
        u.GetComponent<UnitObj>().player = player;
    }

    private void Spawn(GameObject o)
    {
        GameObject spawnVehicle = Instantiate(o, buildingPrefab.transform.position, transform.rotation);

        UnitObj u = spawnVehicle.GetComponent<UnitObj>();
        u.GetComponent<UnitObj>().player = player;
    }

    public override void AddMenuChoises(StrategyPlayerControl p)
    {
        p.DisableAllButtons();
        //p.constructorOption.gameObject.SetActive(true);
        if(onBuildingMode)
        {
            p.dropzoneOptions[0].gameObject.SetActive(true);
            //p.dropzoneOptions[1].gameObject.SetActive(true);
        }
        else
        {
            p.dropzoneOptions[0].gameObject.SetActive(false);
            //p.dropzoneOptions[1].gameObject.SetActive(false);
        }
    }
    
    public override void Combat(){}
}
