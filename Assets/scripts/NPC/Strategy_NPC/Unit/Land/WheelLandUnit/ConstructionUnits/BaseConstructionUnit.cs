﻿using UnityEngine;
using System.Collections;
using System;

public abstract class BaseConstructionUnit : WheelLandUnit
{
    public enum ConstructionWheelLandUnitType { DropZone }
    public ConstructionWheelLandUnitType myConstructionWheelLandUnitType;


    protected bool buildingInprogress = false;
    protected bool buildActive = false;
    protected int buildIndex = 0;
    protected Vector3 buildingPos;
    protected GameObject ghostBuildingPrefab;


    public GameObject buildingPrefab;

    public override void Awake()
    {
        base.Awake();

        myWheelLandUnitType = WheelLandUnitType.Construction;
    }
}
