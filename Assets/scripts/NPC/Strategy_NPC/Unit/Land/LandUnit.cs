﻿using UnityEngine;
using System.Collections;
using System;

public abstract class LandUnit : UnitObj
{
    public enum LandUnitType { Track, Hover, Wheel } ;
    public LandUnitType myLandUnitType;

    public override void Awake()
    {
        base.Awake();

        unitType = UnitType.Land;
    }
}
