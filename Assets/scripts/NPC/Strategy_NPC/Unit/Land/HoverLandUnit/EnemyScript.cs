﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : HoverLandUnit
{
    public StateEnemyBaseClass myAwareness;
    public static float myHeight = 0.5f;

    //Material
    public Material visible;
    public Material invisible;
    
    public Vector3 startPoint;

    private bool validLastSawPos = false;
    public bool GetValidLastSawPos()
    {
        return validLastSawPos;
    }
    public void SetValidLastSawPos(bool v)
    {
        validLastSawPos = v;
    }

    private Vector3 _lastSawPos;
    public void LastSawPos(Vector3 pos)
    {
        if(pos == null)
        {
            validLastSawPos = false;
        }
        else
        {
            validLastSawPos = true;
            _lastSawPos = pos;
        }
        
        //debug
        if(lastSawPosPrefab != null) lastSawPosPrefab.transform.position = pos;
    }
    public Vector3 LastSawPos()
    {
        return _lastSawPos;
    }

    private List<LandUnit> _enemiesLandUnit;
    public List<LandUnit> GetEnemiesLandUnit()
    {
        return _enemiesLandUnit;
    }
    public void SetEnemies(List<LandUnit> enemies)
    {
        _enemiesLandUnit = enemies;

        float cDist;
        float dist = Mathf.Infinity;

        foreach(LandUnit u in _enemiesLandUnit)
        {
            cDist = Vector3.Distance(transform.position, u.transform.position);

            if(cDist <= dist)
            {
                dist = cDist;
                enemyInFocus = u;
                LastSawPos(u.transform.position);
                if(debugState) Debug.Log("new enemy focus set");
            }
        }
    }

    //temp
    GameObject sightRangePrefab;
    GameObject attackRangePrefab;
    GameObject lastSawPosPrefab;
    public bool debugState;
    
    public override void Awake()
    {
        base.Awake();

        index = 2;

        //Type
        moveStyle = FormationStyle.Column;
        roe = ROE.AlwaysAttack;
        myHoverLandUnitType = HoverLandUnitType.Enemy;

        //Map
        minHeight = 0.5f;

        //Movement
        maxVelocity = 15f;
        desiredVelocity = 15f;
        minVelocity = -0.5f;
        cutoffVelocity = 0f;
        maxAcceRate = 10f;
        maxBrakeRate = -10f;
        cutoffAcceBrakeRate = 0.0f;
        movementWaypointAcceptableDistance = 0.1f;
        formationAcceptableDistance = 1f;

        //Turning
        maxRotationSpeed = 5f;
        rotationRate = 2.5f;
        aceptableAngle = 2f;

        //PIDControl
        movementControl = new PIDControl(0.5f, 0.03f, 0f, maxBrakeRate, maxAcceRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        movementControl.debug = false;
        turnControl = new PIDControl(0.5f, 0.03f, 0f, -rotationRate, rotationRate, -cutoffAcceBrakeRate, cutoffAcceBrakeRate);
        turnControl.debug = false;

        //Enemy
        enemyLayer = "Selectable";

        //Combat
        maxHealth = 20;
        health = 20;
        minDefenceRange = 0f;
        attackRange = 200f;
        sightRange = 210f;
        SetDefenceRange(210f);
        searchTime = 1f;
        attackDamage = 0f;
        reloadTime = 2f;
        lastShot = 0.6f;
        turretTurnRate = 2f;
        turretMaxAngle = 15f;
        
        //Side
        side = 2;

        lostCost = 1;
        buyCost = 0;

        //UI
        myAwareness = new StateWandering(this);
        aStar = new AStar();
        startPoint = transform.position;
        
        //temp
        /*Vector3 scale;
        sightRangePrefab = player.DrawCilinder(sightRange, myHeight, transform.position);
        sightRangePrefab.transform.SetParent(transform);
        sightRangePrefab.SetActive(debugState);
        scale = sightRangePrefab.transform.localScale;
        sightRangePrefab.transform.localScale = new Vector3(scale.x, 0.1f, scale.z);
        
        defenceRangePrefab = player.DrawCilinder(attackDamage, myHeight, transform.position);
        defenceRangePrefab.transform.SetParent(transform);
        defenceRangePrefab.SetActive(debugState);
        scale = defenceRangePrefab.transform.localScale;
        defenceRangePrefab.transform.localScale = new Vector3(scale.x, 0.5f, scale.z);
        
        attackRangePrefab = player.DrawCilinder(attackDamage, myHeight, transform.position);
        attackRangePrefab.transform.SetParent(transform);
        attackRangePrefab.SetActive(debugState);
        scale = attackRangePrefab.transform.localScale;
        attackRangePrefab.transform.localScale = new Vector3(scale.x, 1f, scale.z);

        lastSawPosPrefab = player.DrawBall(0.5f, transform.position);
        lastSawPosPrefab.SetActive(debugState);*/
        
    }
    
    public override void Update()
    {
        base.Update();
        lastShot += Time.deltaTime;
        
        myAwareness.Update();
        Combat();
        if(move) MoveAndTurn();
        
    }

    protected override void MovementAnimation() { }

    public void ChangeToInvisible()
    {
        for(int i = 0; i < prefabs.Length; i++)
        {
            //Debug.Log("change to invisible");
            prefabs[i].GetComponent<Renderer>().material = invisible;
        }
        
    }
    
    public void ChangeToVisible()
    {
        for(int i = 0; i < prefabs.Length; i++)
        {
            //Debug.Log("change to visible");
            prefabs[i].GetComponent<Renderer>().material = visible;
        }
    }

    public override void AddMenuChoises(StrategyPlayerControl p){}

    protected override void HitSound()
    {
        sounds[2].Play();
    }

    public override void Sound()
    {
        
    }

    public void ResetRotation()
    {
        
    }

    //temp 
    public void SetColor(Color c)
    {
        
    }
}
