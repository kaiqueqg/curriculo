﻿using UnityEngine;
using System.Collections;
using System;

public class HoverLandUnit : LandUnit
{
    public enum HoverLandUnitType { Enemy }
    public HoverLandUnitType myHoverLandUnitType;

    public override void Awake()
    {
        base.Awake();

        myLandUnitType = LandUnitType.Hover;
    }

    public override void AddMenuChoises(StrategyPlayerControl p) {}

    public override void MoveAndTurn()
    {
        if(wPos < waypoints.Count && Vector3.Distance(waypoints[wPos], transform.position) < movementWaypointAcceptableDistance) // got there
        {
            wPos++;
        }

        Rotate();

        if(wPos < waypoints.Count)
        {
            if(angleBetween.IsBetween(-2f, 2f))
            {
                currentVelocity += maxAcceRate * Time.deltaTime;
                if(currentVelocity > maxVelocity) currentVelocity = maxVelocity;
                transform.position = Vector3.MoveTowards(transform.position, waypoints[wPos], currentVelocity * Time.deltaTime);
            }
        }
        else
        {
            currentVelocity = 0;
        }
    }

    protected void Rotate()
    {
        if(waypoints != null && waypoints.Count > 0)
        {
            //float power;
            Vector3 dir;

            //Deciding last direction
            if(wPos >= waypoints.Count) //last direction
            {
                dir = lastDirection;
            }
            else if(waypoints[wPos] == transform.position) // avoiding vector 0,0,0
            {
                dir = transform.forward;
            }
            else //valid direction
            {
                dir = waypoints[wPos] - transform.position;
            }
            angleBetween = Vector3.Angle(dir, transform.forward);
            transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, dir, 2f * Time.deltaTime, 0.0f));
        }
    }

    public void Brake()
    {
        currentVelocity += (maxBrakeRate * Time.deltaTime);
        if(currentVelocity < 0) currentVelocity = 0;
    }
}
