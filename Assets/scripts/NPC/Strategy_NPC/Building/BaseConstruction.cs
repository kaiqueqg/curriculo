﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseConstruction : BaseDestructibleObj
{
    public GameObject spawnPoint;
    public GameObject firstMove;

    public override void AddMenuChoises(StrategyPlayerControl p){}

    public override void ButtonSelected(int index){}

    public override void ObjectSelected(GameObject o){}
}
