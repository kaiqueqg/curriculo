﻿using UnityEngine;
using System.Collections;

public abstract class BaseDestructibleObj : MonoBehaviour
{
    //health bar
    public GameObject canvasHealthBar;
    private RectTransform green;
    private RectTransform red;

    public StrategyPlayerControl player;
    public Camera c;
    public enum DestructableType { Null, Construction, Unit };
    public float maxHealth;
    public float health;
    public int side;
    public DestructableType destructableType;
    public GameObject selectedBoxPrefab;
    public GameObject destroyedPrefab;

    protected int lostCost;
    protected int buyCost;

    protected AudioSource[] sounds;

    public bool godMode;

    private bool isSelected = false;
    public bool IsSelected() { return isSelected; }
    
    public virtual void Awake()
    {
        
        if(canvasHealthBar != null) green = canvasHealthBar.transform.GetChild(0).GetComponent<RectTransform>();
        if(canvasHealthBar != null) red = canvasHealthBar.transform.GetChild(1).GetComponent<RectTransform>();
        sounds = GetComponents<AudioSource>();
    }

    public virtual void Update()
    {
        if(player != null) c = player.mainCamera;
        UpdateHealthBar();
    }

    protected virtual void HitSound() { }

    public virtual void Hit(float damage)
    {
        HitSound();

        if(!godMode)
        {
            health -= damage;
            UpdateHealthBar();

            if(health <= 0)
            {
                Tank t = GetComponent<Tank>();
                if(t != null) player.tankAvailable--;

                player.PointsUpdate(lostCost);
                if(destroyedPrefab != null) Destroy(Instantiate(destroyedPrefab, gameObject.transform.position, gameObject.transform.rotation), 20f);
                Destroy(gameObject, 0.01f);
                Destroy(this);
            }
        }
    }

    public abstract void ButtonSelected(int index);
    public abstract void ObjectSelected(GameObject o);
    public abstract void AddMenuChoises(StrategyPlayerControl p);
    
    public void SetSelected(bool v)
    {
        isSelected = v;
        if(selectedBoxPrefab != null) selectedBoxPrefab.SetActive(v);
        else { Debug.Log("No selected box prefab..."); }
    }

    private void UpdateHealthBar()
    {
        if(canvasHealthBar != null && c != null)
        {
            //Rotation
            //Vector3 v = c.transform.position - transform.position;
            //v.x = v.z = 0.0f;
            //canvasHealthBar.transform.LookAt(c.transform.position - v);
            //canvasHealthBar.transform.Rotate(90, 0, 0); //only characters need to do 180
            canvasHealthBar.transform.rotation = c.transform.rotation;

            //Size
            float cameraDistanceRate = player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 1, 1);
            float maxCanvasSize = player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 7, 8);
            float greenH = health.Map(0, maxHealth, 0, maxCanvasSize);
            float redH = (maxHealth - health).Map(0, maxHealth, 0, maxCanvasSize);

            green.localPosition = new Vector3(redH / 2, green.localPosition.y, green.localPosition.z);
            green.localScale = new Vector3(greenH, cameraDistanceRate, green.localScale.z);

            red.localPosition = new Vector3(-((maxCanvasSize / 2) - redH / 2), green.localPosition.y, green.localPosition.z);
            red.localScale = new Vector3(redH, cameraDistanceRate, green.localScale.z);
        }
    }
}
