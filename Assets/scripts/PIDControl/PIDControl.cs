﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PIDControl
{
    public bool debug = false;
    private bool firstValueSkip = true;

    private float error = 0f;
    private float lastInput = 0f;

    private float kP;
    public void SetkP(float v) { kP = v;  }

    private float kD;
    public void SetkD(float v) { kD = v;  }

    private float kI;
    public void SetkI(float v) { kI = v;  }

    private float proportional = 0f;
    private float derivative = 0f;
    private List<float> li;
    private float integral = 0f;

    private float acce = 0f;
    private float maxAcceRate;
    private float maxBrakeRate;
    private float minAcceRate;
    private float minBrakeRate;

    public PIDControl(float p,float d,float i, float maxB, float maxA, float minB, float minA)
    {
        li = new List<float>();

        kP = p;
        kD = d;
        kI = i;
        
        maxAcceRate = maxA;
        maxBrakeRate = maxB;
        minAcceRate = minA;
        minBrakeRate = minB;
}
    
    public float GetOutput(float setPoint, float inputValue)
    {
        if(firstValueSkip)
        {
            error = setPoint - inputValue;
            lastInput = inputValue;
            firstValueSkip = false;
            return 0;
        }
        else
        {
            error = setPoint - inputValue;

            //Proportional
            proportional = error;

            //Derivate
            if(error != 0) derivative = (lastInput - inputValue) / Time.deltaTime;
            else derivative = 0;

            //Integral
            li.Add(error);
            if(li.Count > 9) li.RemoveAt(0);
            integral = 0;
            foreach(float n in li) integral += n;

            acce = (proportional * kP) + (derivative * kD) + (integral * kI);
            acce = Mathf.Clamp(acce, maxBrakeRate, maxAcceRate);
            if(acce < minAcceRate && acce > minBrakeRate) acce = 0;
            if(debug) Debug.Log(Time.time + "\t" + inputValue + "\t" + acce + "\t" + error + "\t" + (proportional * kP) + "\t" + (derivative * kD) + "\t" + (integral * kI));

            lastInput = inputValue;
        }
        return acce;
    }
}
