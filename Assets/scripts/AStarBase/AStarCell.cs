﻿using UnityEngine;

public class AStarCell
{
    public enum T { START, END, ANY, OPEN, EVA }
    public T type;

    public bool resetFlag = true;

    private float minHeight = 0;
    public float MinHeight() { return minHeight; }
    public void MinHeight(float w)
    {
        minHeight = w;
    }

    private Vector3 pos = new Vector3(-1f,-1f,-1f);
    public Vector3 Pos() { return pos; }
    public void Pos(Vector3 p)
    {
        pos = p;
    }

    private AStarCell parent = null;
    public AStarCell Parent() { return parent; }
    public void Parent(AStarCell p)
    {
        parent = p;
    }

    private float g = 0;
    public float G()
    {
        if(parent != null)
        {
            return g + parent.G();
        }
        else
        {
            return g;
        }
    }
    public void G(float g)
    {
        this.g = g;
        float displayG;
        if(parent == null)
        {
            displayG = this.g;
        }
        else
        {
            displayG = g+parent.G();
        }
    }

    private float h = 0;
    public float H() { return h; }
    public void H(float h)
    {
        this.h = h;
    }

    public float F()
    {
        float displayF = G()+H();
        return displayF;
    }
}
