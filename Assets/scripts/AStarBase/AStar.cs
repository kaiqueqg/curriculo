﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AStar{

    public AStarCell[,] GetMap(int mapSize, float precision, float[] heightsToTest,int mask)
    {
        AStarCell[,] map;
        Collider[] co;
        Vector3 halfExtents = new Vector3((1*precision/2)-(0.01f * precision), (1*precision/2)- (0.01f * precision), (1 * precision/2)- (0.01f * precision));
        map = new AStarCell[(int)(mapSize/precision),(int)(mapSize/precision)];

        for(int i = 0; i < map.GetLength(0); i++)
        {
            for(int j = 0; j < map.GetLength(1); j++)
            {
                AStarCell cell = new AStarCell();
                cell.Pos(new Vector3(i,0f,j));
                cell.type = AStarCell.T.ANY;
                cell.MinHeight(-1);
                for(int k = 0; cell.MinHeight() == -1 && k < heightsToTest.GetLength(0); k++)
                {
                    co = Physics.OverlapBox(new Vector3(i*precision,heightsToTest[k],j*precision),halfExtents,Quaternion.identity, mask);
                    
                    if(co.Length == 0)
                    {
                        cell.MinHeight(heightsToTest[k]);
                    }

                    map[i,j] = cell;
                }
            }
        }

        return map;
    }

    public float MinHeight(AStarCell[,] map, float x, float z, float precision)
    {
        int realX = (int)(Math.Round(x / precision));
        int realZ = ((int)Math.Round(z / precision));
        return map[realX, realZ].MinHeight();
    }

    public List<Vector3> FindPath(Vector3 s, Vector3 e, float precision, bool changeEndPos, float minHeight, AStarCell[,] map, LayerMask mask)
    {
        s = s / precision;
        e = e / precision;

        for(int i = 0; i < map.GetLength(0); i++)
        {
            for(int j = 0; j < map.GetLength(1); j++)
            {
                map[i,j].resetFlag = true;
            }
        }

        AStarCell startCell;
        AStarCell endCell;
        AStarCell current = new AStarCell();
        List<Vector3> rtnList = new List<Vector3>();
        List<AStarCell> openCells = new List<AStarCell>();
        List<AStarCell> evaCells = new List<AStarCell>();
        int mapSize = map.GetLength(0);
        
        int x = (int)(Math.Round(s.x));
        int z = (int)(Math.Round(s.z));
        if(x<0) x=0;
        if(x>mapSize-1) x=mapSize-1;
        if(z<0) z=0;
        if(z>mapSize-1) z=mapSize-1;

        startCell = map[x,z];
        startCell.type = AStarCell.T.START;
        startCell.resetFlag = false;
        startCell.Parent(null);
        startCell.G(0f);
        startCell.H(Vector3.Distance(s,e));
        openCells.Add(startCell);
        
        endCell = map[(int)(Math.Round(e.x)),(int)(Math.Round(e.z))];
        endCell.type = AStarCell.T.END;
        endCell.resetFlag = false;  
        endCell.Parent(null);
        endCell.G(Vector3.Distance(startCell.Pos(),endCell.Pos()));
        endCell.H(0f);
        
        if(x == Math.Round(e.x) && z == Math.Round(e.z))
        {
            //Debug.Log("AStar: Start and End Rounded positions are equal! x:" + x + " z:" + Math.Round(e.z));
            if(changeEndPos)
            {
                rtnList.Add(new Vector3(e.x * precision, e.y, e.z * precision));
            }

            return rtnList;
        }

        List<AStarCell> n;
        bool w = false;
        while(!w)
        {
            current = GetLowest(openCells);
            if(current != null)
            {
                openCells.Remove(current);
                evaCells.Add(current);
                current.type = AStarCell.T.EVA;

                n = GetNeighbors(current.Pos(), map);

                for(int i = 0; i < n.Count; i++)
                {
                    if(n[i].MinHeight() <= minHeight && n[i].MinHeight() != -1)
                    {
                        float d = Vector3.Distance(current.Pos(),n[i].Pos());
                        if(n[i].resetFlag)
                        {
                            n[i].resetFlag = false;
                            n[i].Parent(current);
                            openCells.Add(n[i]);
                            n[i].type = AStarCell.T.OPEN;
                            n[i].G(d);
                            n[i].H(Vector3.Distance(endCell.Pos(),n[i].Pos()));
                        }
                        else if(n[i].type == AStarCell.T.END)
                        {
                            n[i].Parent(current);
                            current = n[i];
                            w = true;
                            break;
                        }
                        else if(n[i].type == AStarCell.T.ANY)
                        {
                            n[i].Parent(current);
                            n[i].type = AStarCell.T.OPEN;
                            openCells.Add(n[i]);
                            n[i].G(d);
                        }
                        else if(n[i].type == AStarCell.T.OPEN)
                        {
                            if(Math.Round(n[i].F(), 2) > Math.Round(current.G() + d + n[i].H(), 2))
                            {
                                n[i].Parent(current);
                                n[i].G(d);
                            }
                        }
                        else if(n[i].type == AStarCell.T.EVA)
                        {
                            if(n[i].Parent() != null && Math.Round(n[i].F(),2) > Math.Round(current.G() + d + n[i].H(),2))
                            {
                                n[i].Parent(current);
                                n[i].G(d);
                            }
                        }
                        
                    }
                }
            }
            else
            {
                w = true;
            }
        }

        if(current != null)
        {
            rtnList.Add(new Vector3(current.Pos().x * precision, current.Pos().y, current.Pos().z * precision));
            while(current.Parent() != null)
            {
                current = current.Parent();
                rtnList.Add(new Vector3(current.Pos().x * precision, current.Pos().y, current.Pos().z * precision));
            }
            rtnList[rtnList.Count-1] = new Vector3(s.x * precision, s.y, s.z * precision);
            
            rtnList.Reverse();

            if(changeEndPos) rtnList[rtnList.Count-1] = new Vector3(e.x*precision, e.y, e.z*precision);
        }
        else
        {
            Debug.Log("No Path to: " + new Vector3(e.x*precision, e.y, e.z * precision) + "from: " + new Vector3(s.x * precision, s.y, s.z * precision));
        }

        
        //return rtnList;
        return RemovePoints(rtnList, precision, mask);
    }

    private List<AStarCell> GetNeighbors(Vector3 p, AStarCell[,] map)
    {
        List<AStarCell> r = new List<AStarCell>();

        if(p.x > 0)
        {
            r.Add(map[(int)p.x-1,(int)p.z]);
            /*
            if(p.z > 0)
            {
                r.Add(map[(int)p.x-1,(int)p.z-1]);
            }
            if(p.z < map.GetLength(1)-1)
            {
                r.Add(map[(int)p.x-1,(int)p.z+1]);
            }*/
        }
        if(p.x < map.GetLength(0)-1)
        {
            r.Add(map[(int)p.x+1,(int)p.z]);
            /*
            if(p.z > 0)
            {
                r.Add(map[(int)p.x+1,(int)p.z-1]);
            }
            if(p.z < map.GetLength(1)-1)
            {
                r.Add(map[(int)p.x+1,(int)p.z+1]);
            }*/
        }
        if(p.z < map.GetLength(1)-1)
        {
            r.Add(map[(int)p.x,(int)p.z+1]);
        }
        if(p.z > 0)
        {
            r.Add(map[(int)p.x,(int)p.z-1]);
        }

        return r;
    }

    private List<Vector3> RemovePoints(List<Vector3> l, float precision, LayerMask mask)
    {
        if(l.Count > 2)
        {
            int c = 0;
            Vector3 s;
            Vector3 e;

            s = l[c];
            c+=2;

            while(c < l.Count)
            {
                e = l[c];

                Vector3 direction = (e-s).normalized;
                direction = Quaternion.Euler(0,90,0) * direction;
                Vector3 posA = s + (direction * 0.4513149f * precision);
                Vector3 posAA = e + (direction * 0.4513149f * precision);
                Vector3 posB = s - (direction * 0.4513149f * precision);
                Vector3 posBB = e - (direction * 0.4513149f * precision);

                bool testA = Physics.Linecast(s,e,mask);
                bool testAA = Physics.Linecast(posA,posAA,mask);
                bool testBB = Physics.Linecast(posB,posBB,mask);
                
                if(!testA && !testAA && !testBB && c < l.Count)
                {
                    l.RemoveAt(c-1);
                }
                else
                {
                    s = l[c-1];
                    c++;
                }
            }
        }
        return l;
    }

    private List<Vector3> RemoveIntermediaryPoints(List<Vector3> l, LayerMask mask)
    {
        List<Vector3> n = new List<Vector3>();

        int i = 0;
        Vector3 s = l[i];
        n.Add(l[i]);

        if(l.Count > 1)
        {
            i++;
            Vector3 e = l[i];

            while(i < l.Count-1)
            {
                e = l[i];

                bool test = true;
                while(test)
                {
                    test = !Physics.Linecast(s,e,mask);
                    if(test)
                    {
                        Vector3 direction = (e-s).normalized;

                        direction = Quaternion.Euler(0,90,0) * direction;
                        Vector3 posA = s + (direction * 0.49f);
                        Vector3 posB = s - (direction * 0.49f);
                        Vector3 posAA = e + (direction * 0.49f);
                        Vector3 posBB = e - (direction * 0.49f);

                        test = !Physics.Linecast(posA,posAA,mask);

                        if(test)
                        {
                            test = !Physics.Linecast(posB,posBB,mask);

                            if(test)
                            {
                                if(i+1 >= l.Count)
                                {
                                    i++;
                                    test = false;
                                }
                                else
                                {
                                    i++;
                                    e = l[i];
                                }
                            }
                        }
                    }
                }
                n.Add(l[i-1]);
                s = l[i-1];
            }
        }
        
        return n;
    }
    
    private AStarCell GetLowest(List<AStarCell> list)
    {
        if(list.Count == 0) return null;

        AStarCell rtn;
        rtn = list[0];
        for(int i = 1; i < list.Count; i++)
        {
            if(Math.Round(list[i].F()) < Math.Round(rtn.F())) rtn = list[i];
            else if(Math.Round(list[i].F()) == Math.Round(rtn.F()))
            {
                if(Math.Round(list[i].H()) < Math.Round(rtn.H())) rtn = list[i];
            }
        }

        return rtn;
    }
}
