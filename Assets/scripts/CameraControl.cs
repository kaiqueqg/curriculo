﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraControl
{
    public StrategyPlayerControl player;
    public Camera camera;
    
    private float minH;
    private BaseDestructibleObj.DestructableType selectedType;
    private List<BaseDestructibleObj> destructablesSelected;
    private UnitObj lider;
    private List<UnitObj> followers;
    private List<UnitObj> supportFollowers;

    private float holdSpaceTime;
    private float holdSpaceMinTime;
    private float holdMouse0Time;
    private float holdMouse0MinTime;

    private bool isSettingDefence;
    private Vector3 defencePos;
    private Vector3 defenceDir;
    private float defenceDis;

    private CameraControl(){}
    public CameraControl(StrategyPlayerControl p)
    {
        player = p; camera = p.mainCamera;

        minH = -1f;
        selectedType = BaseDestructibleObj.DestructableType.Null;
        destructablesSelected = new List<BaseDestructibleObj>();
        followers = new List<UnitObj>();
        supportFollowers = new List<UnitObj>();

        holdSpaceTime = 0;
        holdSpaceMinTime = 0.7f;
        holdMouse0Time = 0;
        holdMouse0MinTime = 0.2f;

        player.unitOptions[0].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.roe.ToString();
        player.unitOptions[1].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.moveStyle.ToString();
        player.unitOptions[2].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.defendStyle.ToString();
        
        DisableAll();
    }

    public void Update()
    {
        player.pointsText.GetComponent<Text>().text = player.GetPoints().ToString();

        if(player.gameStarted)
        {
            //Camera movement
            if(Input.GetKey(KeyCode.W))
            {
                player.cameraContainer.transform.Translate(Vector3.forward *
                    player.cameraMovementRate *
                    player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 1f, 1.2f));
            }
            if(Input.GetKey(KeyCode.S))
            {
                player.cameraContainer.transform.Translate(-Vector3.forward *
                    player.cameraMovementRate *
                    player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 1f, 1.2f));
            }
            if(Input.GetKey(KeyCode.A))
            {
                player.cameraContainer.transform.Translate(Vector3.left *
                    player.cameraMovementRate *
                    player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 1f, 1.2f));
            }
            if(Input.GetKey(KeyCode.D))
            {
                player.cameraContainer.transform.Translate(-Vector3.left *
                    player.cameraMovementRate *
                    player.cameraDistance.Map(player.cameraMinZoom, player.cameraMaxZoom, 1f, 1.2f));
            }
            if(Input.GetKey(KeyCode.Q))
            {
                player.cameraContainer.transform.Rotate(Vector3.up, -1f);
            }
            if(Input.GetKey(KeyCode.E))
            {
                player.cameraContainer.transform.Rotate(Vector3.up, 1f);
            }
            if(player.mainCamera.transform.position.y > player.cameraMinZoom && Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                player.mainCamera.transform.Translate(player.zoomRate * Vector3.forward);
                player.cameraDistance = player.mainCamera.transform.position.y.Map(player.cameraMinZoom, player.cameraMaxZoom, 0, 100);
            }
            if(player.mainCamera.transform.position.y < player.cameraMaxZoom && Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                player.mainCamera.transform.Translate(player.zoomRate * -Vector3.forward);
                player.cameraDistance = player.mainCamera.transform.position.y.Map(player.cameraMinZoom, player.cameraMaxZoom, 0, 100);
            }
            
            if(player.cameraContainer.transform.position.x > player.mapSize)
                player.cameraContainer.transform.position = new Vector3(player.mapSize, player.cameraContainer.transform.position.y, player.cameraContainer.transform.position.z);

            if(player.cameraContainer.transform.position.z > player.mapSize)
                player.cameraContainer.transform.position = new Vector3(player.cameraContainer.transform.position.x, player.cameraContainer.transform.position.y, player.mapSize);

            if(player.cameraContainer.transform.position.x < 0)
                player.cameraContainer.transform.position = new Vector3(0, player.cameraContainer.transform.position.y, player.cameraContainer.transform.position.z);

            if(player.cameraContainer.transform.position.z < 0)
                player.cameraContainer.transform.position = new Vector3(player.cameraContainer.transform.position.x, player.cameraContainer.transform.position.y, 0);
        }
        
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            //player.unitOptions[4].GetComponent<Image>().color = Color.red;
        }
        if(Input.GetKeyUp(KeyCode.Alpha1))
        {
            //player.unitOptions[4].GetComponent<Image>().color = Color.white;
            ButtonPressed(1);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            //player.unitOptions[3].GetComponent<Image>().color = Color.red;
        }
        if(Input.GetKeyUp(KeyCode.Alpha2))
        {
            //player.unitOptions[3].GetComponent<Image>().color = Color.white;
            ButtonPressed(2);
        }
        
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            //player.unitOptions[2].GetComponent<Image>().color = Color.red;
        }
        if(Input.GetKeyUp(KeyCode.Alpha3))
        {
            //player.unitOptions[2].GetComponent<Image>().color = Color.white;
            ButtonPressed(3);
        }
        
        //Hold and Stop
        if(Input.GetKeyDown(KeyCode.Space))
        {
            //Debug.Log("Space Down");
            holdSpaceTime = Time.time;
            //player.unitOptions[3].GetComponent<Image>().color = Color.red;

            UnitObj current;
            foreach(BaseDestructibleObj o in destructablesSelected)
            {
                current = o.GetComponent<UnitObj>();
                if(current != null)
                {
                    current.move = !current.move;
                }
            }
        }
        if(Input.GetKey(KeyCode.Space))
        {
            //Debug.Log("Space");
            if(Time.time - holdSpaceTime >= holdSpaceMinTime)
            {
                //Debug.Log("spaceHold");
                //player.unitOptions[0].GetComponent<Image>().color = Color.red;
            }
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            //Debug.Log("SpaceUp");
            if(Time.time - holdSpaceTime >= holdSpaceMinTime)
            {
                //player.unitOptions[0].GetComponent<Image>().color = Color.red;

                UnitObj current;
                foreach(BaseDestructibleObj o in destructablesSelected)
                {
                    current = o.GetComponent<UnitObj>();
                    if(current != null)
                    {
                        current.Stop();
                    }
                }
            }
            //player.unitOptions[0].GetComponent<Image>().color = Color.white;
            //player.unitOptions[1].GetComponent<Image>().color = Color.white;
        }

        //Unselect
        if(Input.GetKey(KeyCode.Mouse1))
        {
            foreach(BaseDestructibleObj o in destructablesSelected)
            {
                o.SetSelected(false);
                
            }
            destructablesSelected = new List<BaseDestructibleObj>();
            selectedType = BaseDestructibleObj.DestructableType.Null;
            DisableAll();
        }

        //Hold Mouse0
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            holdMouse0Time = Time.time;
            isSettingDefence = false;
        }
        if(Input.GetKey(KeyCode.Mouse0)) //Setting defence ghosts!
        {
            if(Time.time - holdMouse0Time > holdMouse0MinTime)
            {
                RaycastHit h;
                Ray r = camera.ScreenPointToRay(Input.mousePosition);

                if(!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(r, out h, Mathf.Infinity, LayerMask.GetMask("Floor")))
                {
                    DiscriminateUnits();

                    //Setting first pos
                    if(!isSettingDefence)
                    {
                        defencePos = h.point;
                        
                        if(selectedType == BaseDestructibleObj.DestructableType.Unit)
                        {
                            Vector3[] pos = player.ArrowFormation(defencePos, Vector3.forward, followers.Count, supportFollowers.Count, lider.minHeight);

                            lider.defencePos = defencePos;
                            //lider.SetDefenceRange(lider.minDefenceRange);
                            lider.ShowDefencePos(true);

                            int posIndex = 0;
                            for(int i = 0; i < followers.Count; i++, posIndex++)
                            {
                                followers[i].defencePos = pos[posIndex];
                                //followers[i].SetDefenceRange(followers[i].minDefenceRange);
                                followers[i].ShowDefencePos(true);
                            }
                            for(int i = 0; i < supportFollowers.Count; i++, posIndex++)
                            {
                                supportFollowers[i].defencePos = pos[posIndex];
                                //supportFollowers[i].SetDefenceRange(supportFollowers[i].minDefenceRange);
                                supportFollowers[i].ShowDefencePos(true);
                            }
                        }

                        isSettingDefence = true;
                    }
                    else //Updating Pos
                    {
                        Vector3[] pos = new Vector3[0];

                        switch(player.defendStyle)
                        {
                            case UnitObj.DefendStyle.Arrow:
                                pos = player.ArrowFormation(defencePos, (h.point - defencePos).normalized, followers.Count, supportFollowers.Count, lider.minHeight);
                                break;
                            case UnitObj.DefendStyle.Line:
                                pos = player.LineFormation(defencePos, (h.point - defencePos).normalized, followers.Count, supportFollowers.Count, lider.minHeight);
                                break;
                            default:
                                pos = player.LineFormation(defencePos, (h.point - defencePos).normalized, followers.Count, supportFollowers.Count, lider.minHeight);
                                break;
                        }
                        defenceDir = h.point - defencePos;
                        defenceDis = defenceDir.magnitude;

                        lider.SetDefenceRange(defenceDis);
                        lider.UpdateDefencePos(defenceDir);

                        int posIndex = 0;
                        for(int i = 0; i < followers.Count; i++, posIndex++)
                        {
                            followers[i].defencePos = pos[posIndex];
                            followers[i].SetDefenceRange(defenceDis);
                            followers[i].UpdateDefencePos(defenceDir);
                        }
                        for(int i = 0; i < supportFollowers.Count; i++, posIndex++)
                        {
                            supportFollowers[i].defencePos = pos[posIndex];
                            supportFollowers[i].SetDefenceRange(defenceDis);
                            supportFollowers[i].UpdateDefencePos(defenceDir);
                        }
                    }
                }
            }
            else
            {
                isSettingDefence = false;
            }
        }

        //Selection
        if(Input.GetKey(KeyCode.LeftShift)) //multiple selection
        {
            //Debug.Log("Shift");
            if(Input.GetKeyUp(KeyCode.Mouse0))
            {
                RaycastHit h;
                Ray r = camera.ScreenPointToRay(Input.mousePosition);

                if(!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(r, out h, Mathf.Infinity, LayerMask.GetMask("Selectable")))
                {
                    BaseDestructibleObj selection = h.collider.GetComponent<BaseDestructibleObj>();

                    //no type
                    if(selectedType == BaseDestructibleObj.DestructableType.Null)
                    {
                        selectedType = selection.destructableType;
                        selection.SetSelected(true);
                        destructablesSelected.Add(h.collider.gameObject.GetComponent<BaseDestructibleObj>());
                        MenuUpdate();
                    }
                    //same type
                    else if(selection.destructableType == selectedType) 
                    {
                        selection.SetSelected(true);
                        destructablesSelected.Add(h.collider.gameObject.GetComponent<BaseDestructibleObj>());
                    }
                    //changing type
                    else
                    {
                        ResetSelected();
                        selectedType = selection.destructableType;
                        selection.SetSelected(true);
                        destructablesSelected.Add(h.collider.gameObject.GetComponent<BaseDestructibleObj>());
                        MenuUpdate();
                    }

                    DiscriminateUnits();
                }
            }
        }
        else if(Input.GetKeyUp(KeyCode.Mouse0)) //single selection
        {
            RaycastHit h;
            Ray r = camera.ScreenPointToRay(Input.mousePosition);

            if(!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(r, out h))
            {
                if(h.collider.gameObject.layer == LayerMask.NameToLayer("Floor"))
                {
                    if(destructablesSelected.Count > 0 && selectedType == BaseDestructibleObj.DestructableType.Unit)
                    {
                        List<UnitObj> units = new List<UnitObj>();
                        UnitObj uO;
                        foreach(BaseDestructibleObj o in destructablesSelected)
                        {
                            uO = o.GetComponent<UnitObj>();
                            if(uO != null) units.Add(uO);
                        }

                        DiscriminateUnits();

                        if(isSettingDefence)
                        {
                            isSettingDefence = false;

                            lider.isLider = false;
                            lider.movingInFormation = false;
                            lider.ShowDefencePos(false);
                            lider.MoveToPoint(lider.defencePos, defenceDir);
                            foreach(UnitObj u in followers)
                            {
                                u.movingInFormation = false;
                                u.ShowDefencePos(false);
                                u.MoveToPoint(u.defencePos, defenceDir);
                            }
                            foreach(UnitObj u in supportFollowers)
                            {
                                u.movingInFormation = false;
                                u.ShowDefencePos(false);
                                u.MoveToPoint(u.defencePos, defenceDir);
                            }
                        }
                        else
                        {
                            lider.MoveAsLider(followers, supportFollowers, h.point);
                        }
                    }
                }
                else
                {
                    BaseDestructibleObj selection = h.collider.GetComponent<BaseDestructibleObj>();
                    if(selection != null)
                    {
                        if(selectedType == BaseDestructibleObj.DestructableType.Null)
                        {
                            selectedType = selection.destructableType;
                            selection.SetSelected(true);
                            destructablesSelected.Add(selection);
                            MenuUpdate();
                        }
                        //else if(selection.destructableType == selectedType)
                        //{
                        //    selection.SetSelected(true);
                        //    destructablesSelected.Add(h.collider.gameObject.GetComponent<BaseDestructibleObj>());
                        //}
                        else
                        {
                            ResetSelected();
                            selectedType = selection.destructableType;
                            selection.SetSelected(true);
                            destructablesSelected.Add(selection);
                            MenuUpdate();
                        }

                        DiscriminateUnits();
                    }
                    else
                    {
                        Debug.Log("Selection null");
                    }
                }
            }
        }
    }

    private void DiscriminateUnits()
    {
        if(selectedType == BaseDestructibleObj.DestructableType.Unit && destructablesSelected.Count > 0)
        {
            UnitObj l = null;
            List<UnitObj> f = new List<UnitObj>();
            List<UnitObj> supportF = new List<UnitObj>();

            for(int i = 0; i < destructablesSelected.Count; i++)
            {
                if(destructablesSelected[i].GetComponent<Tank>() != null)
                {
                    f.Add(destructablesSelected[i].GetComponent<UnitObj>());
                }
                else if(destructablesSelected[i].GetComponent<BaseConstructionUnit>() != null)
                {
                    supportF.Add(destructablesSelected[i].GetComponent<UnitObj>());
                }
            }

            if(f.Count > 0)
            {
                lider = f[0];
                f.RemoveAt(0);

                followers = new List<UnitObj>();
                for(int i = 0; i < f.Count; i++)
                {
                    followers.Add(f[i]);
                }
                supportFollowers = new List<UnitObj>();
                for(int i = 0; i < supportF.Count; i++)
                {
                    supportFollowers.Add(supportF[i]);
                }
            }
            else if(supportF.Count > 0)
            {
                lider = supportF[0];
                supportF.RemoveAt(0);

                followers = new List<UnitObj>();
                for(int i = 0; i < supportF.Count; i++)
                {
                    followers.Add(supportF[i]);
                }
            }

        }
        else
        {
            Debug.Log("Not unit selected");
        }
        
    }

    private void ResetSelected()
    {
        DisableAll();

        foreach(BaseDestructibleObj o in destructablesSelected)
        {
            o.SetSelected(false);
        }
        destructablesSelected = new List<BaseDestructibleObj>();
        selectedType = BaseDestructibleObj.DestructableType.Null;
    }

    private void MenuUpdate()
    {
        DisableAll();
        
        foreach(BaseDestructibleObj o in destructablesSelected)
        {
            o.AddMenuChoises(player);
        }

        if(selectedType == BaseDestructibleObj.DestructableType.Unit)
        {
            foreach(Button b in player.unitOptions)
            {
                b.gameObject.SetActive(true);
            }
        }
        else if(selectedType == BaseDestructibleObj.DestructableType.Construction)
        {
            
        }
    }

    private void DisableAll()
    {
        player.constructorOption.gameObject.SetActive(false);
        foreach(Button b in player.unitOptions)
        {
            b.gameObject.SetActive(false);
        }
        foreach(Button b in player.constructionOptions)
        {
            b.gameObject.SetActive(false);
        }
        foreach(Button b in player.dropzoneOptions)
        {
            b.gameObject.SetActive(false);
        }
    }

    public void ButtonPressed(int index)
    {
        switch(index)
        {
            case 1:
                player.roe = player.roe.Forward();
                player.unitOptions[0].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.roe.ToString();
                break;
            case 2:
                player.moveStyle = player.moveStyle.Forward();
                player.unitOptions[1].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.moveStyle.ToString();
                break;
            case 3:
                player.defendStyle = player.defendStyle.Forward();
                player.unitOptions[2].transform.GetChild(1).GetChild(0).GetComponent<Text>().text = player.defendStyle.ToString();
                break;
        }

        foreach(BaseDestructibleObj o in destructablesSelected)
        {
            o.ButtonSelected(index);
        }
    }
}
