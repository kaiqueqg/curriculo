﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropDestroy : MonoBehaviour {

    public ParticleSystem destroyingParticles;
    public AudioSource destroyingSound;
    public GameObject objectToSpawn;
    private bool gotHit = false;

    public void OnTriggerEnter(Collider other)
    {
        if(!gotHit && (other.gameObject.layer == LayerMask.NameToLayer("Selectable") || other.gameObject.layer == LayerMask.NameToLayer("Enemy")))
        {
            gotHit = true;
            //Debug.Log("Destroy");

            Renderer r = GetComponent<MeshRenderer>();
            if(r != null) r.enabled = false;

            Transform[] ts = gameObject.GetComponentsInChildren<Transform>();
            for(int i = 0; i < ts.Length; i++)
            {
                r = ts[i].GetComponent<MeshRenderer>();
                if(r != null) r.enabled = false;
            }

            if(objectToSpawn != null) Destroy(Instantiate(objectToSpawn, gameObject.transform), 2f);
            if(destroyingParticles != null) Destroy(Instantiate(destroyingParticles, gameObject.transform), 2f);

            destroyingSound.Play();
            Destroy(gameObject, 2f);
        }
    }
}
