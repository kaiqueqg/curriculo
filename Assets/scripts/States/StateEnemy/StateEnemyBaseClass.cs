﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class StateEnemyBaseClass
{
    public EnemyScript e;
    
    public abstract void StateEntry();
    public abstract void StateExit();
    public abstract void Update();
    public abstract void Search();
    
    protected List<LandUnit> FindLandUnitsInRange(float range, LayerMask mask)
    {
        List<LandUnit> returnObject = new List<LandUnit>();
        Collider[] co = Physics.OverlapCapsule(e.raycastPoint.position, e.raycastPoint.position, range, mask);

        if(co.Length > 0)
        {
            Vector3 enemyPos;
            RaycastHit h;
            for(int i = 0; i < co.Length; i++)
            {
                enemyPos = co[i].transform.position;

                LandUnit u;
                if(Physics.Raycast(e.raycastPoint.position, enemyPos - e.raycastPoint.position, out h/*, range, LayerMask.GetMask(e.enemyLayer)*/))
                {
                    u = h.collider.GetComponent<LandUnit>();
                    if(u != null && u.side != e.side)
                    {
                        returnObject.Add(u);
                    }
                }
            }
        }

        return returnObject;
    }

    protected GameObject GetClosestEnemy(List<GameObject> enemies)
    {
        float closestDistance = e.sightRange + 0.01f;
        GameObject closestEnemy = null;

        foreach(GameObject o in enemies)
        {
            float dist = Vector3.Distance(e.transform.position, o.transform.position);
            if(closestDistance > dist)
            {
                closestDistance = dist;
                closestEnemy = o;
            }
        }

        return closestEnemy;
    }
}
