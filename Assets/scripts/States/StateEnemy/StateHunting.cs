﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StateHunting : StateEnemyBaseClass
{
    private float lastSearchTime;

    public StateHunting(EnemyScript en)
    {
        e = en;
        StateEntry();
    }

    public override void StateEntry()
    {
        e.searchTime = 0.5f;
        lastSearchTime = 2f;
        e.attack = false;

        e.moveAnim.SetBool("State", true);
        e.ChangeToVisible();

        if(e.debugState) Debug.Log("Hunting");
    }

    public override void StateExit(){}

    public override void Update()
    {
        lastSearchTime += Time.deltaTime;
        if(lastSearchTime > e.searchTime)
        {
            lastSearchTime = 0;
            Search();
        }
    }

    public override void Search()
    {
        if(e.debugState) Debug.Log("Hunt Search");
        List<LandUnit> l = FindLandUnitsInRange(e.sightRange, LayerMask.GetMask(e.enemyLayer));

        if(l.Count > 0)
        {
            e.SetEnemies(l);

            if(e.enemyInFocus != null)
            {
                float dist = Vector3.Distance(e.enemyInFocus.transform.position, e.transform.position);
                if(e.debugState) Debug.Log("Hunt decision");
                if(dist < e.GetDefenceRange() && dist > e.attackRange)
                {
                    if(e.debugState) Debug.Log("Go hunt");
                    Hunt();
                }
                else if(dist < e.attackRange)
                {
                    if(e.debugState) Debug.Log("To fight state");
                    e.myAwareness = new StateFight(e);
                }
            }
        }
        else
        {
            if(!e.GetValidLastSawPos())
            {
                if(e.debugState) Debug.Log("back to wandering");
                e.myAwareness = new StateWandering(e);
            }
            else
            {
                Hunt();
            }
        }
    }

    private void Hunt()
    {
        if(Vector3.Distance(e.transform.position, e.LastSawPos()) > e.movementWaypointAcceptableDistance)
        {
            if(e.debugState) Debug.Log("On the hunt");
            e.SetValidLastSawPos(true);
            e.MoveToPoint(e.LastSawPos());
        }
        else
        {
            if(e.debugState) Debug.Log("Hunt LastSawPos checked");
            e.SetValidLastSawPos(false);
        }
    }

    private bool CheckEnemyInFocus()
    {
        float dist = Vector3.Distance(e.enemyInFocus.transform.position, e.transform.position);
        if(e.enemyInFocus != null && dist > e.GetDefenceRange() && dist < e.sightRange)
        {
            RaycastHit h;
            if(Physics.Raycast(new Vector3(e.transform.position.x, e.transform.position.y + 0.1f, e.transform.position.z), e.enemyInFocus.transform.position - e.transform.position, out h, dist))
            {
                LandUnit u = h.collider.GetComponent<LandUnit>();
                if(u != null && u.side != e.side)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
