﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StateFight : StateEnemyBaseClass
{
    private float lastSearchTime;

    public StateFight(EnemyScript en)
    {
        e = en;
        StateEntry();
    }

    public override void StateEntry()
    {
        e.searchTime = 0.1f;
        lastSearchTime = 1f;
        e.attack = true;

        e.moveAnim.SetBool("State", true);
        e.ChangeToVisible();

        if(e.debugState) Debug.Log("Fight");
        e.Stop();
    }

    public override void StateExit() {}

    public override void Update()
    {
        lastSearchTime += Time.deltaTime;
        if(lastSearchTime > e.searchTime)
        {
            lastSearchTime = 0;
            Search();
            e.Combat();
        }
    }

    public override void Search()
    {
        if(e.debugState) Debug.Log("Fight Search");
        List<LandUnit> l = FindLandUnitsInRange(e.sightRange, LayerMask.GetMask("Selectable"));

        if(l.Count > 0)
        {
            e.SetEnemies(l);
            if(e.enemyInFocus != null && Vector3.Distance(e.enemyInFocus.transform.position, e.transform.position) > e.attackRange)
            {
                if(e.debugState) Debug.Log("Fight Enemy out of defence range");
                e.myAwareness = new StateHunting(e);
            }
            else
            {
                e.transform.LookAt(e.enemyInFocus.transform);
            }
        }
        else
        {
            if(!e.GetValidLastSawPos())
            {
                if(e.debugState) Debug.Log("From Fight to Wandering");
                e.myAwareness = new StateWandering(e);
            }
            else
            {
                if(e.debugState) Debug.Log("From Fight to Hunting");
                e.myAwareness = new StateHunting(e);
            }
        }
    }
}
