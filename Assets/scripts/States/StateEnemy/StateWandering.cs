﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class StateWandering : StateEnemyBaseClass
{
    float lastSearchTime;

    public StateWandering(EnemyScript en)
    {
        e = en;
        StateEntry();
    }

    public override void StateEntry()
    {
        e.searchTime = 1f;
        lastSearchTime = 2f;
        e.attack = false;

        if(Vector3.Distance(e.transform.position, e.startPoint) < e.movementWaypointAcceptableDistance)
        {
            e.moveAnim.SetBool("State", false);
            e.ChangeToInvisible();
        }
        else
        {
            e.moveAnim.SetBool("State", true);
            e.ChangeToVisible();
        }

        if(e.debugState)Debug.Log("Wandering");
    }

    public override void StateExit(){}

    public override void Update()
    {
        if(Vector3.Distance(e.transform.position, e.startPoint) < e.movementWaypointAcceptableDistance)
        {
            e.moveAnim.SetBool("State", false);
            e.ChangeToInvisible();
        }
        else
        {
            e.moveAnim.SetBool("State", true);
            e.ChangeToVisible();
        }
            
        lastSearchTime += Time.deltaTime;
        if(lastSearchTime > e.searchTime)
        {
            lastSearchTime = 0;
            Search();
        }
    }

    public override void Search()
    {
        if(e.debugState) Debug.Log("Wandering Search");
        
        List<LandUnit> lu = FindLandUnitsInRange(e.sightRange, LayerMask.GetMask(e.enemyLayer));
        e.SetEnemies(lu);

        if(lu.Count > 0)
        {
            if(e.debugState) Debug.Log("wandering Found");
            
            if(Vector3.Distance(e.enemyInFocus.transform.position, e.transform.position) > e.attackRange)
            {
                if(e.debugState) Debug.Log("to hunting state");
                e.myAwareness = new StateHunting(e);
            }
            else
            {
                e.myAwareness = new StateFight(e);
            }

        }
        else
        {
            if(Vector3.Distance(e.transform.position, e.startPoint) > 0.1f) e.MoveToPoint(e.startPoint);
        }
    }
}
